package Lesson11_Absrtraction.Task3;

public class OrcGrunt extends Orc {

    public OrcGrunt() {
        this.setDamage(10);
        this.setArmor(150);
        this.setAttack("hacking");
        this.setHp(50);
    }

    @Override
    void attack() {
        System.out.printf("Attack! *%s* %.1f!\n", getAttack(), getDamage());
    }
}
