package Lesson11_Absrtraction.Task3;

/**
 *  Создать абстрактный класс “Orc” с переменными для показателей урона, брони, здоровья и типа атаки, содержащий также абстрактный метод Attack.
 *  Наследовать от него 3 обычных класса “OrcPeon”, “OrcGrunt” и “OrcShaman”,
 *  для каждого из которых установить тип атаки ”crushing”, “hacking” и “magical”(как переменная класса типа String),
 *  также определить для каждого из классов переменные из абстрактного класса и функцию Attack, чтобы она выводила на экран показатель атаки и ее тип.
 */

public abstract class Orc {

    private double damage;
    private double armor;
    private double hp;
    private String attack;

    abstract void attack ();

    public double getDamage() {
        return damage;
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }

    public double getArmor() {
        return armor;
    }

    public void setArmor(double armor) {
        this.armor = armor;
    }

    public double getHp() {
        return hp;
    }

    public void setHp(double hp) {
        this.hp = hp;
    }

    public String getAttack() {
        return attack;
    }

    public void setAttack(String attack) {
        this.attack = attack;
    }
}
