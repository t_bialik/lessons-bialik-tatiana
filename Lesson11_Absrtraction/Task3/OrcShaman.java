package Lesson11_Absrtraction.Task3;

public class OrcShaman extends Orc {

    public OrcShaman() {
        this.setDamage(50);
        this.setArmor(30);
        this.setAttack("magical");
        this.setHp(50);
    }

    @Override
    void attack() {
        System.out.printf("Attack! *%s* %.1f!\n", getAttack(), getDamage());
    }
}
