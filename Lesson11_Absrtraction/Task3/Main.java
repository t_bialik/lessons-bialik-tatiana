package Lesson11_Absrtraction.Task3;

public class Main {
    public static void main(String[] args) {
        Orc shaman = new OrcShaman();
        Orc grunt = new OrcGrunt();
        Orc peon = new OrcPeon();

        shaman.attack();
        grunt.attack();
        peon.attack();
    }
}
