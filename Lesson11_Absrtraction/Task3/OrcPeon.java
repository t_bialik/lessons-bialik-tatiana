package Lesson11_Absrtraction.Task3;

public class OrcPeon extends Orc {
    public OrcPeon() {
        this.setDamage(20);
        this.setArmor(100);
        this.setHp(100);
        this.setAttack("crushing");
    }

    @Override
    void attack() {
        System.out.printf("Attack! *%s* %.1f!\n", getAttack(), getDamage());
    }
}
