package Lesson11_Absrtraction.Task1;

/**
 * Создать абстрактный класс “Dog” с переменными для клички, пола, веса собак, содержащий также абстрактные методы Voice и Action.
 * Наследовать от него три обычных класса “DomesticDog”, “ServiceDog” и “HomelessDog”,
 * для каждого из которых определить строковую переменную address, organization, area соответственно
 */


public abstract class Dog {

    String name;
    boolean isMale;
    double weight;

    public Dog(String name, boolean isMale, double weight) {
        this.name = name;
        this.isMale = isMale;
        this.weight = weight;
    }

    abstract void voice ();
    abstract void action ();
}
