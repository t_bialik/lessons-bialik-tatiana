package Lesson11_Absrtraction.Task1;

public class ServiceDog extends Dog {
    String organization;

    public ServiceDog(String name, boolean isMale, double weight, String organization) {
        super(name, isMale, weight);
        this.organization = organization;
    }

    @Override
    void voice() {

    }

    @Override
    void action() {

    }
}
