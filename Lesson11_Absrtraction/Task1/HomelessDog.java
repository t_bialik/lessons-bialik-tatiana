package Lesson11_Absrtraction.Task1;

public class HomelessDog extends Dog {
    String area;

    public HomelessDog(String name, boolean isMale, double weight, String area) {
        super(name, isMale, weight);
        this.area = area;
    }

    @Override
    void voice() {

    }

    @Override
    void action() {

    }
}
