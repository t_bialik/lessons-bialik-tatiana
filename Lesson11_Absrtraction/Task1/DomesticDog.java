package Lesson11_Absrtraction.Task1;

public class DomesticDog extends Dog {
    String address;

    public DomesticDog(String name, boolean isMale, double weight, String address) {
        super(name, isMale, weight);
        this.address = address;
    }

    @Override
    void voice() {
        System.out.println("Wuf!");
    }

    @Override
    void action() {
        System.out.println("*sitting*");
    }
}
