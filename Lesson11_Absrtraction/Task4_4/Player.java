package Lesson11_Absrtraction.Task4_4;

public interface Player {

    String getName ();
    int getMove ();
    void addPoint();
    int getPoints ();
    void decision();
}
