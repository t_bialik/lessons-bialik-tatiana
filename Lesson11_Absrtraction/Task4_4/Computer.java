package Lesson11_Absrtraction.Task4_4;

import java.util.Random;

public class Computer implements Player {

    private String name;
    private int move;
    private int points;

    Computer() {
        this.setName(RandomName.randomChoice());
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMove(int move) {
        this.move = move;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public String getName() {
        return name;
    }

    public void printMove() {
        if (this.move == 1)
            System.out.println("stone");
        if (this.move == 2)
            System.out.println("scissors");
        if (this.move == 3)
            System.out.println("paper");
    }

    @Override
    public int getMove() {
        return move;
    }

    @Override
    public void addPoint() {
        System.out.println(name + " won this turn");
        points++;
    }

    @Override
    public int getPoints() {
        return points;
    }

    @Override
    public void decision() {
        Random random = new Random();
        this.move = random.nextInt(3) + 1;
    }
}
