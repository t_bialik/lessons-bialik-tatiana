package Lesson11_Absrtraction.Task4_4;

import java.util.Scanner;

public class Human implements Player {

    private String name;
    private int move;
    private int points;

    public void setName(String name) {
        this.name = name;
    }

    public void setMove(int move) {
        this.move = move;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public String getName() {
        return name;
    }

    public void printMove() {
        if (this.move == 1)
            System.out.println("stone");
        if (this.move == 2)
            System.out.println("scissors");
        if (this.move == 3)
            System.out.println("paper");
    }

    @Override
    public int getMove() {

        return move;
    }


    @Override
    public void addPoint() {
        System.out.println(name + " won this turn");
        points++;
    }

    @Override
    public int getPoints() {
        return points;
    }

    @Override
    public void decision() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\033[36mWhat's your turn?\033[m\n1 - stone\n2 - scissors\n3 - paper");
        this.move = scanner.nextInt();
    }
}
