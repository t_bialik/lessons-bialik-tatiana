package Lesson11_Absrtraction.Task4_4;

import java.util.Scanner;

public class Game {

    Human playerOne;
    Computer playerTwo;

    Game() {
        this.playerOne = new Human();
        this.playerTwo = new Computer();
    }

    void play() {
        System.out.println("\033[36mInput your name please\033[m");
        Scanner scanner = new Scanner(System.in);
        playerOne.setName(scanner.nextLine());

        System.out.printf("\n\033[33m%s\033[m vs \033[35m%s\033[m\n\n", playerOne.getName(),playerTwo.getName());

        while (true) {
            playerOne.decision();
            playerTwo.decision();

            if (playerOne.getMove() == 1) {
                if (playerTwo.getMove() == 2) {
                    playerOne.addPoint();
                }

                if (playerTwo.getMove() == 3) {
                    playerTwo.addPoint();
                }
            }

            if (playerOne.getMove() == 2) {

                if (playerTwo.getMove() == 3) {
                    playerOne.addPoint();
                }

                if (playerTwo.getMove() == 1) {
                    playerTwo.addPoint();
                }
            }

            if (playerOne.getMove() == 3) {
                if (playerTwo.getMove() == 2) {
                    playerTwo.addPoint();
                }

                if (playerTwo.getMove() == 1) {
                    playerOne.addPoint();
                }
            }

            if (playerOne.getMove() != 1 && playerOne.getMove() != 2 && playerOne.getMove() != 3)
                System.out.println("Please try again!");

            else {
                System.out.print("\033[33mYour turn: \033[m");
                playerOne.printMove();

                System.out.print("\033[35m" + playerTwo.getName() + "'s turn: \033[m");
                playerTwo.printMove();

                printPoints();

                System.out.println("\n\033[36mInput any symbol to continue or input \"n\" to exit\033[m");
                String decide = scanner.nextLine();
                if (decide.equalsIgnoreCase("n")) {
                    if (playerOne.getPoints() > playerTwo.getPoints())
                        System.out.println(playerOne.getName() + " won!");
                    else if (playerOne.getPoints() < playerTwo.getPoints())
                        System.out.println(playerTwo.getName() + " won!");
                    else
                        System.out.println("Nobody won :(");

                    break;
                }
            }
        }
    }

    void printPoints() {
        System.out.printf("\033[33m%s\033[m %d : %d \033[35m%s\033[m\n", playerOne.getName(), playerOne.getPoints(), playerTwo.getPoints(), playerTwo.getName());
    }
}

