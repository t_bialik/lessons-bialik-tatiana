package Lesson11_Absrtraction.Task2;

public class Chinchilla extends Rodent{
    public Chinchilla(String sex, double weight, String color, double speed) {
        super(sex, weight, color, speed);
    }

    @Override
    void run() {
        System.out.printf("It's a chinchilla! Its speed is %.1f!\n", speed);
    }

    @Override
    void jump() {

    }

    @Override
    void eat() {

    }
}
