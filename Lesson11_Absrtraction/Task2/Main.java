package Lesson11_Absrtraction.Task2;

public class Main {
    public static void main(String[] args) {
        Rodent hamster = new Hamsteer("male", 10.5, "white", 10.2);
        Rodent rat = new Rat("female", 13.2, "black", 19.3);
        Rodent chinchilla = new Chinchilla("male", 25.3, "brown", 19.8);

        hamster.run();
        rat.run();
        chinchilla.run();
    }
}
