package Lesson11_Absrtraction.Task2;

/**
 *   Создать абстрактный класс “Rodent” с переменными для пола, веса, цвета шкуры и скорости бега,
 *   содержащий также абстрактные методы Run, Jump и Eat.
 *   Наследовать от него 3 обычных класса “Hamster”, “Chinchilla” и “Rat”,
 *   для каждого из которых переопределить метод Run, чтобы он выводил название класса и скорость бега грызуна на экран
 */

public abstract class Rodent {

    String sex;
    double weight;
    String color;
    double speed;

    abstract void run ();
    abstract void jump ();
    abstract void eat();

    public Rodent(String sex, double weight, String color, double speed) {
        this.sex = sex;
        this.weight = weight;
        this.color = color;
        this.speed = speed;
    }
}
