package Lesson11_Absrtraction.Task4;

import java.util.Scanner;

public class Human extends Player {

    @Override
    void decision() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\033[36mWhat's your turn?\033[m\n1 - stone\n2 - scissors\n3 - paper");
        this.setMove(scanner.nextInt());
    }

}
