package Lesson11_Absrtraction.Task4;

public abstract class Player {

    private String name;
    private int move;
    private int points;

    abstract void decision ();

    void printMove () {
        if (this.move == 1)
            System.out.println("stone");
        if (this.move == 2)
            System.out.println("scissors");
        if (this.move == 3)
            System.out.println("paper");
    }

    void addPoint () {
        System.out.println(name + " won this turn!");
        points++;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMove() {
        return move;
    }

    public void setMove(int move) {
        this.move = move;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
