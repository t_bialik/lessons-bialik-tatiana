package Lesson11_Absrtraction.Task4;

import java.util.Random;

public class Computer extends Player {

    Computer() {
        this.setName(RandomName.randomChoice());
    }



    void decision() {
        Random random = new Random();
        this.setMove(random.nextInt(3) + 1);
    }
}
