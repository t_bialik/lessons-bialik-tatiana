package Lesson13;

public class Druid extends Elf {
    public Druid(int hp, int speed, int damage) {
        super(hp, speed, damage);
    }

    Druid () {

    }

    @Override
    void say() {
        System.out.println("For the nature!");
    }

    Druid [] createArray (int size) {
        Druid [] druids = new Druid[size];
        for (int i = 0; i < size; i++) {
            druids[i] = new Druid(10,10,10);
        }
        return druids;
    }
}
