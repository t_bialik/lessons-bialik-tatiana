package Lesson13;

public class Main {

    public static  <T> void printElfs (T [] elfs) {
        System.out.println("\nThis is " + elfs[0].getClass().getSimpleName());
        for (T elf : elfs) {
            System.out.println(elf.toString());
        }
    }

    public static void main(String[] args) {
        Warrior warrior = new Warrior();
        Archer archer = new Archer();
        Druid druid = new Druid();
        printElfs(warrior.createArray(5));
        printElfs(archer.createArray(5));
        printElfs(druid.createArray(5));
    }
}
