package Lesson13;

public class Warrior extends Elf {
    public Warrior(int hp, int speed, int damage) {
        super(hp, speed, damage);
    }

    Warrior () {

    }

    @Override
    void say() {
        System.out.println("Argh!");
    }

    Warrior [] createArray (int size) {
        Warrior [] warriors = new Warrior[size];
        for (int i = 0; i < size; i++) {
            warriors[i] = new Warrior(20,20,20);
        }
        return warriors;
    }
}
