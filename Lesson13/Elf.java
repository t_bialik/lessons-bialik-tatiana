package Lesson13;

public abstract class Elf<T extends Elf> {

    private int hp;
    private int speed;
    private int damage;
    private String name;

    public Elf() {

    }

    public Elf(int hp, int speed, int damage) {
        this.hp = hp;
        this.speed = speed;
        this.damage = damage;
        this.name = RandomName.randomChoice();
    }

    abstract void say();


    public String toString() {
        return "I am " + getClass().getSimpleName() + " " + name + ".\nMy HP: " + hp + "\nMy SPEED: " + speed + "\nMy DAMAGE: " + damage;
    }

}


