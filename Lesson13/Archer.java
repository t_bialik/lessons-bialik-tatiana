package Lesson13;

public class Archer extends Elf {
    public Archer(int hp, int speed, int damage) {
        super(hp, speed, damage);
    }

    Archer () {

    }

    @Override
    void say() {
        System.out.println("I'm archer");
    }

    Archer [] createArray (int size) {
        Archer [] archers = new Archer[size];
        for (int i = 0; i < size; i++) {
            archers[i] = new Archer(10,10,10);
        }
        return archers;
    }
}
