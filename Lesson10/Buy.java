package Lesson10;

public class Buy extends Product{

    private int num;

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public Buy (String name, double price, double weight, int num) {
        super(name, price, weight);
        this.num = num;
    }

    double allPrice () {
        return super.getPrice() * num;
    }

    double allWeight () {
        return super.getWeight() * num;
    }

    /**
     * методы, возвращающие общий вес и общую цену на основе num
     */
}
