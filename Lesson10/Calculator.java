package Lesson10;

import java.util.Scanner;

public class Calculator {

    private double x;

    public Calculator(double x) {
        this.x = x;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    double add (double y) {
        return x + y;
    }

    double substract (double y) {
        return x - y;
    }

    double multiply (double y) {
        return x * y;
    }

    double divide (double y) {
        return x / y;
    }

    double sqrt () {
        return Math.sqrt(x);
    }

    double sin () {
        return Math.sin(x);
    }

    static void run () {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input your number");
        Calculator x = new Calculator(scanner.nextDouble());

        while (true) {
            System.out.println("Input parameter");
            double y = scanner.nextDouble();
            System.out.println("Choose operation \nIf you want to change a number input \"c\"");
            String operation = scanner.next();

            if (operation.equalsIgnoreCase("q"))
                break;

            if (operation.equalsIgnoreCase("+"))
                System.out.println(x.add(y));

            if (operation.equalsIgnoreCase("-"))
                System.out.println(x.substract(y));

            if (operation.equalsIgnoreCase("*"))
                System.out.println(x.multiply(y));

            if (operation.equalsIgnoreCase("/"))
                System.out.println(x.divide(y));

            if (operation.equalsIgnoreCase("sqrt"))
                System.out.println(x.sqrt());

            if (operation.equalsIgnoreCase("sin"))
                System.out.println(x.sin());

            if (operation.equalsIgnoreCase("c")) {
                System.out.println("Input your new number");
                x.setX(scanner.nextDouble());
            }
        }
    }
}

class InnerCalculator extends Calculator {

    private double memory;

    public double getMemory() {
        return memory;
    }

    public void setMemory(double memory) {
        this.memory = memory;
    }

    double add () {
        return getX() + memory;
    }

    public InnerCalculator(double x) {
        super(x);
    }
}
