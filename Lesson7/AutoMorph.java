package Lesson7;

import java.util.Scanner;

public class AutoMorph {
    public static void main(String[] args) {
        System.out.println("Введите границу промежутка");
        Scanner scan = new Scanner(System.in);
        long bord = scan.nextLong();
        checkIt (bord);
    }

    static void checkIt (long numb)
    {
        for (long i = 0; i < numb; i++) {
            String check = Long.toString(i);
            String number = i * i + "";
            if (number.endsWith(check))
                System.out.println(i);
        }
    }
}
