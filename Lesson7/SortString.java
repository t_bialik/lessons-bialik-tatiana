package Lesson7;

import java.util.Arrays;
import java.util.Scanner;

public class SortString {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Введи строку");
        String string = scan.nextLine();
        String [] str = string.split(" ");

        sortString(str);

        for (int i = 0; i < str.length; i++) {
            System.out.print(str[i] + " ");
        }
    }

    static void sortString (String [] str)
    {
        int [] help = new int [str.length];

        for (int i = 0; i < str.length; i++)
            for (int j = 1; j < 9; j++)
                if (str[i].contains(Integer.toString(j)))
                    help [i] = j;

        Arrays.sort(help);

        for (int i = 0; i < str.length; i++)
            for (int j = 0; j < str.length; j++)
                if (str[i].contains(Integer.toString(help[j])))
                {
                    String p = str[i];
                    str[i] = str [j];
                    str [j] = p;
                }
    }
}
