package Lesson14;

// Создать функцию, в которую передается логическое, либо целочисленное значение, после чего распознается тип переданных данных.
// Если было передано логическое значение false, либо целочисленное значение 0, функция возвращает строку “Верно”,
// во всех остальных случаях возвращается строка “Неверно”.

public class Task2 {

    static <T> String function (T num) {
        if (num instanceof Boolean && !(Boolean)num || num instanceof Integer && (Integer)num == 0) {
            return "Верно";
        }
        else
            return "Неверно";
    }
}
