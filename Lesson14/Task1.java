package Lesson14;

//Создать функцию, в которую передается либо целочисленное, либо вещественное, либо строковое значение,
// после чего на экран выводится тип переданных в функцию данных и само значение.

public class Task1 {

    static <T> void function (T num) {
        System.out.println(num.getClass().getSimpleName() + " " + num);
    }
}
