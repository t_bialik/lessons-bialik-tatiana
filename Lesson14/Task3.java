package Lesson14;

//Создать функцию, в которую передаются два значения, каждое из которых может являться либо целочисленным, либо вещественным,
//после чего определяется тип, и функция возвращает их сумму либо в виде целочисленного, либо в виде вещественного значения.

public class Task3 {

    static <T extends  Number> T function (T num1, T num2) {

        T result = null;

        if (num1 instanceof Double && num2 instanceof Double) {
            Double tmp = (Double)num1 + (Double)num2;
            result = (T)tmp;
            return result;
        }
        else {
            Integer tmp = (Integer)num1 + (Integer)num2;
            result = (T)tmp;
        }

        return result;
    }
}
