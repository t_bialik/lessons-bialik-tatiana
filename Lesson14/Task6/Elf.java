package Lesson14.Task6;

public abstract class Elf {

    private int hp;
    private int damage;

    abstract void strike();

    @Override
    public String toString() {
        return getClass().getSimpleName() + " HP: " + hp + " DAMAGE: " + damage;
    }
}
