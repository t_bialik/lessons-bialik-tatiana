package Lesson14.Task6;

// Создать класс “Elf” с полями для показателей здоровья и урона и функцией “Strike” типа void,
// от которого наследовать 3 класса “ElfWarrior”, “ElfArcher”, “ElfMage”, в каждом из которых переопределить функцию “Strike”,
// также для каждого из классов переопределить функцию toString, через которую будет выводиться информация о классе.
// Написать классобобщение, который принимает на вход только класс “Elf” и его потомков.

public class Task6 {
    public static void main(String[] args) {
        CommonClass cl = new CommonClass();
        cl.elf = new ElfArcher();
    }
}
