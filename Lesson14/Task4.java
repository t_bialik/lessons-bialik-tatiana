package Lesson14;

// Создать функцию, в которую передается либо символьное, либо строковое значение, после чего вычисляется и возвращается код символа, либо сумма кодов символов.


public class Task4 {
    static <T> int function (T symb) {
        int result = 0;

        if (symb instanceof String) {
            char [] array = ((String)symb).toCharArray();

            for (int i = 0; i < array.length; i++) {
                result += (int)array[i];

            }
        }
        else
            result = (int)(Character) symb;

        return result;
    }
}
