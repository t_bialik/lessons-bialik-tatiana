package Lesson9;

import java.io.FileReader;

public class Fraction {

    private double denom;
    private double num;

    public Fraction(double denom, double num) {
        this.denom = denom;
        this.num = num;
    }


    public Fraction() {
    }

    public double getDenom() {
        return denom;
    }

    public double getNum() {
        return num;
    }

    public void setDenom(double denom) {
        this.denom = denom;
    }

    public void setNum(double num) {
        this.num = num;
    }

    void print () {
        System.out.printf("%.2f/%.2f\n", num, denom);
    }
}

