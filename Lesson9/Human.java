package Lesson9;

public class Human {

    private String name;
    private String surname;
    private int age;
    private boolean isWoman;

    public Human() {
    }

    public Human(String name, String surname, int age, boolean isWoman) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.isWoman = isWoman;
    }

    public Human(String name) {
        this.name = name;
    }

    public Human(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }
}
