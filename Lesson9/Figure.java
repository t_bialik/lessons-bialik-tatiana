package Lesson9;

import java.util.Scanner;

public class Figure {

    private double x;
    private double y;
    private double z;
    private String type;

    static void run () {

        System.out.println("\n\n\033[35mTASK 7\033[m");
        Figure line = new Figure(3);
        Figure rectangle = new Figure(3,4);
        Figure paralellepiped = new Figure (3,4,5);
        System.out.println(line.getType() + "\n" + rectangle.getType() + "\n" + paralellepiped.getType());
    }

    public Figure(double x) {
        this.x = x;
        this.type = "It's a line";
    }

    public Figure(double x, double y) {
        this.x = x;
        this.y = y;
        this.type = "It's a rectangle";
    }

    public Figure(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.type = "It's a paralellepiped";
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public String getType() {
        return type;
    }
}
