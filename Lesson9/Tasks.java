package Lesson9;

public class Tasks {
    public static void main(String[] args) {
        Point point = new Point(3, 2);
        Point point2 = new Point(4,8);
        System.out.println("\033[35mTASK 1:\033[m your distance is " + point.distance(point2));

        FractionLogic.run();

        Triangle.run();

        Monster.run();

        Function.run();

        Figure.run();

        Polinomial.run();

        Date.run();
    }


}
