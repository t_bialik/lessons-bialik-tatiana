package Lesson9;

public class Monster {
    private String name;
    private int health;
    private double attack;
    private double protect;

    public Monster(String name, int health, double attack, double protect) {
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.protect = protect;
    }

    static void run () {
        System.out.println("\n\033[35mTASK 5\033[m");
        Monster monster = new Monster("Vlad", 100, 33, 25);
        monster.print();
    }

    void print () {
        System.out.printf("\033[32mMonster %s \033[m\n\033[32mHealth:\033[m %d \n\033[32mAttack:\033[m %.2f \n\033[32mProtect:\033[m %.2f", name, health, attack, protect);
    }
}
