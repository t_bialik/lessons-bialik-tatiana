package Lesson9;

public class Date {

    private int day;
    private int month;
    private int year;

    static void run () {
        System.out.println("\n\n\033[35mTASK 9\033[m");
        Date dateOne = new Date(12,10,2018);
        Date dateTwo = new Date(13,4,2018);
        dateOne.printDate();
        System.out.println("\nThis is " + dateOne.season());
        System.out.printf("It's %d day in the year\n", dateOne.whichDay());
        dateTwo.printDate();
        System.out.println("\nThis is " + dateTwo.season());
        System.out.printf("It's %d day in the year\n", dateTwo.whichDay());
        System.out.print("From ");
        dateOne.printDate();
        System.out.print(" to ");
        dateTwo.printDate();
        System.out.printf(" is %d days\n", dateOne.fromDayToDay(dateTwo));

        Date dateThree = new Date(23,1,2018);
        dateThree.printDate();
        System.out.println("\nThis is " + dateThree.season());
        System.out.printf("It's %d day in the year\n", dateThree.whichDay());
        Date dateFour = new Date(30,6,2018);
        dateFour.printDate();
        System.out.println("\nThis is " + dateFour.season());
        System.out.printf("It's %d day in the year\n", dateFour.whichDay());

    }

    String season () {

        if (this.month < 3 || this.month == 12)
            return "\033[31mWinter\033[m";
        else if (this.month < 6 && this.month > 2)
            return "\033[33mSpring\033[m";
        else if (this.month < 9 && this.month > 5)
            return "\033[34mSummer\033[m";
        else if (this.month < 12 && this.month > 8)
            return "\033[35mAutumn\033[m";

        return null;
    }

    int whichDay () {
        int result = 0;
        int [] daysOfMonths = {31,28,31,30,31,30,31,31,30,31,30,31};

        if (this.year % 4 == 0)
            daysOfMonths [1] = 29;

        for (int i = 0; i < this.month - 1; i++) {
            result += daysOfMonths [i];
        }

        result += this.day;

        return result;
    }

    int fromDayToDay (Date dayTwo) {
        int result = 0;
        int tmp = 0;

        if (this.year == dayTwo.year)
            result = Math.abs(this.whichDay() - dayTwo.whichDay());

        else if (this.year > dayTwo.year) {
            result = (this.year - dayTwo.year) * 365 + this.whichDay() + (365 - dayTwo.whichDay());
            for (int i = dayTwo.year; i < this.year; i++) {
                if (i % 4 == 0)
                    tmp++;
            }
            result += tmp;
        }

        else {
            result = (dayTwo.year - this.year) * 365 + dayTwo.whichDay() + (365 - this.whichDay());
            for (int i = this.year; i < dayTwo.year; i++) {
                if (i % 4 == 0)
                    tmp++;
            }
            result += tmp;
        }

        return result;
    }

    void printDate () {
        System.out.printf("\033[32m%d.%d.%d\033[m", day, month, year);
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }
}
