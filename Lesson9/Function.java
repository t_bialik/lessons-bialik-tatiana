package Lesson9;

public class Function {

    private double a;
    private double b;
    private double c;
    private double d;

    public Function(double a, double b, double c, double d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }


    static void run () {
        System.out.println("\n\n\033[35mTASK 6\033[m");
        Function function = new Function(3, 5, 1, 4);
        function.printFunction();
        function.function(14);
    }

    void function (int x) {
        System.out.printf("Function value for x = %d: %.2f", x, a * Math.pow(x, 3) + b * Math.pow(x, 2) + c * x + d);
    }

    void printFunction () {
        System.out.printf("Your function: %.1fx^3 + %.1fx^2 + %.1fx + %.1f\n", a, b, c, d);
    }
}
