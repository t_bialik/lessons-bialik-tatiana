package Lesson9;

public class FractionLogic {

    static void run (){
        Fraction frac = new Fraction(3,5);
        Fraction frac1 = new Fraction(10,15);
        System.out.println("\n\033[35mTASK 2: \033[m");
        frac.print();
        frac1.print();
        System.out.print("\033[32mMultiply result:\033[m ");
        FractionLogic.fracMult(frac,frac1).print();
        System.out.print("\033[32mSum result:\033[m ");
        FractionLogic.fracSum(frac,frac1).print();
        System.out.print("\033[32mSubstract result:\033[m ");
        FractionLogic.fracSubtract(frac,frac1).print();
        System.out.print("\033[32mDivide result:\033[m ");
        FractionLogic.fracDivide(frac,frac1).print();
    }

    static Fraction fracMult (Fraction f1, Fraction f2) {
        Fraction result = new Fraction();
        result.setNum(f1.getNum() * f2.getNum());
        result.setDenom(f1.getDenom() * f2.getDenom());
        return result;
    }

    static Fraction fracDivide (Fraction f1, Fraction f2) {
        Fraction result = new Fraction();
        result.setNum(f1.getNum() / f2.getDenom());
        result.setDenom(f1.getDenom() / f2.getNum());
        return result;
    }

    static Fraction fracSum (Fraction f1, Fraction f2) {
        Fraction result = new Fraction();
        if (f1.getDenom() != f2.getDenom()) {
            result.setDenom(f1.getDenom() * f2.getDenom());
            result.setNum(f1.getNum() * f2.getDenom() + f2.getNum() * f1.getDenom());
        }
        else
        {
            result.setDenom(f1.getDenom());
            result.setNum(f1.getNum() + f2.getNum());
        }
        return result;
    }

    static Fraction fracSubtract (Fraction f1, Fraction f2) {
        Fraction result = new Fraction();
        if (f1.getDenom() != f2.getDenom()) {
            result.setDenom(f1.getDenom() * f2.getDenom());
            result.setNum(f1.getNum() * f2.getDenom() - f2.getNum() * f1.getDenom());
        }
        else
        {
            result.setDenom(f1.getDenom());
            result.setNum(f1.getNum() - f2.getNum());
        }
        return result;
    }
}
