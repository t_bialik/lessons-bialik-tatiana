package Lesson9;

import java.util.Scanner;

public class Polinomial {
    private double a;
    private double b;
    private double c;
    private double d;
    private double e;
    private double f;

    static void run () {
        System.out.println("\n\n\033[35mTASK 8\033[m");
        Scanner scan = new Scanner(System.in);
        System.out.println("Put 6 numbers");
        double a = scan.nextDouble();
        double b = scan.nextDouble();
        double c = scan.nextDouble();
        double d = scan.nextDouble();
        double e = scan.nextDouble();
        double f = scan.nextDouble();
        System.out.println("Put x value");
        int x = scan.nextInt();

        Polinomial polinomial = new Polinomial(a,b,c,d,e,f);
        polinomial.printPolinom();
        System.out.println("Result for x = " + x + ": " + polinomial.polinom(x));
        System.out.println("Derivative: " + polinomial.derivat(x));
    }

    double polinom (int x) {
        return a * Math.pow(x, 5) + b * Math.pow(x, 4) + c * Math.pow(x, 3) + d * Math.pow(x, 2) + e * x + f;
    }

    double derivat (int x) {
        return a * 5 * Math.pow(x, 4) + b * 4 * Math.pow(x, 3) + c * 3 * Math.pow(x, 2) + d * 2 * x + e;
    }

    void printPolinom () {
        System.out.printf("Your polinomial is %.1fx^5 + %.1fx^4 + %.1fx^3 + %.1fx^2 + %.1fx + %.1f\n", a, b, c, d, e, f);
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }

    public double getD() {
        return d;
    }

    public double getE() {
        return e;
    }

    public double getF() {
        return f;
    }

    public Polinomial(double a, double b, double c, double d, double e, double f) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }
}
