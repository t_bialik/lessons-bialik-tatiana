package Lesson9;

import java.util.Scanner;

public class Triangle {
    private double a;
    private double b;
    private double c;

    public Triangle() {
    }

    static int run () {
        Scanner scan = new Scanner(System.in);
        System.out.printf("\n\033[35mTASK 3:\033[m\n");
        System.out.println("Put triangle size");
        double a = scan.nextDouble();
        double b = scan.nextDouble();
        double c = scan.nextDouble();
        Triangle triangle = new Triangle(a,b,c);
        if (triangle.getA() == 0 || triangle.getB() == 0 || triangle.getC() == 0)
            return 0;
        System.out.printf("\033[34mYour triangle\033[m \na = %.2f b = %.2f c = %.2f \n\033[34mPerimeter: \033[m%.2f \n\033[34mArea: \033[m%.2f \n\033[34mType: \033[m",
                triangle.getA(), triangle.getB(), triangle.getC(), triangle.perimeter(), triangle.geron());
        triangle.typeTriangle();
        return 0;
    }

    double perimeter (){
        return this.a * this.b * this.c;
    }

    double geron () {
        double hezip = this.perimeter() / 2;
        return Math.sqrt(hezip * (hezip - this.a) * (hezip - this.b) * (hezip - this.c));
    }

    void typeTriangle () {
        if (this.a == this.b && this.b == this.c)
            System.out.println("Equilateral triangle");
        else if (this.a == this.b || this.a == this.c || this.b == this.c)
            System.out.println("Isosceles triangle");
        else System.out.println("Simple triangle");
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    public Triangle(double a, double b, double c) {
        if (a + b < c || a + c < b || b + c < a)
            System.out.println("Triangle doesn't exists");
        else {
            this.a = a;
            this.b = b;
            this.c = c;
        }
    }
}
