package Lesson8_First_classes;

public class Monster {

    String name;
    int health;
    double attack;
    double protect;

    Monster (String name, int health, double attack, double protect) {
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.protect = protect;
    }

    void info () {
        System.out.printf("\nName: %s \nHealth: %d \nAttack: %.2f \nProtect: %.2f\n", name, health, attack, protect);
    }

}
