package Lesson8_First_classes;

public class MyBook {

    String name;
    String author;
    int year;

    MyBook () {

    }

    MyBook(String name, String author, int year) {
        this.name = name;
        this.author = author;
        this.year = year;
    }

    void print () {
        System.out.printf("\n\"%s\" \nAuthor: %s \nYear: %d \n", name, author, year);
    }
}
