package Lesson8_First_classes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HomeLibrary {

    String name;
    String author;
    int year;

    HomeLibrary(String name, String author, int year) {
        this.name = name;
        this.author = author;
        this.year = year;
    }

    void print() {
        System.out.printf("\n\033[33m\"%s\"\n\033[m\033[36mAuthor:\033[m %s \n\033[35mYear:\033[m   %s\n", name, author, year);
    }  //print a book data

    static void printAll(ArrayList<HomeLibrary> lib) {
        for (HomeLibrary book : lib) {
            book.print();
        }
    }//print all books

    static ArrayList<HomeLibrary> initLib() throws IOException {
        ArrayList<HomeLibrary> Library = new ArrayList<>();

        List<String> lines = ReadFileBase.reader("src\\Lesson8_First_classes\\st_names.txt");

        for (int i = 0; i < lines.size() - 2; i += 3) {
            Library.add(new HomeLibrary(lines.get(i), lines.get(i + 1), Integer.parseInt(lines.get(i + 2))));
        }

        return Library;
    } //fill a library from a file

    static void addBook(ArrayList<HomeLibrary> lib) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a book name");
        String name = scan.nextLine();
        System.out.println("Write an author's name");
        String author = scan.nextLine();
        System.out.println("Write an year");
        int year = scan.nextInt();
        lib.add(new HomeLibrary(name, author, year));
    } //add one book

    static void findAuthor(ArrayList<HomeLibrary> lib) {
        boolean flag = false;
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a book or an author name");
        String authorName = scan.nextLine();
        for (HomeLibrary i : lib) {
            if (i.author.equals(authorName) || i.name.equals(authorName)) {
                i.print();
                flag = true;
            }
        }

        if (!flag)
            System.out.println("\033[31mNo such books or author :(\033[m");
    } //find a book with author/name

    static void findYear(ArrayList<HomeLibrary> lib) {
        Scanner scan = new Scanner(System.in);
        boolean flag = false;
        System.out.println("Write an year");
        int year = scan.nextInt();
        for (HomeLibrary i : lib) {
            if (i.year == year) {
                i.print();
                flag = true;
            }
        }

        if (!flag)
            System.out.println("\033[31mNo such years :(\033[m");
    } //find a book with year

    static void delBook(ArrayList<HomeLibrary> lib) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a book or an author name");
        String name = scan.nextLine();
        for (int i = 0; i < lib.size(); i++) {
            if (lib.get(i).author.equals(name) || lib.get(i).name.equals(name)) {
                lib.remove(i);
            }
        }
    }  //delete an author or a book

    static void sortYear (ArrayList<HomeLibrary> lib) {
        for (int i = 0; i < lib.size(); i++) {
            for (int j = 0; j < lib.size() - 1; j++) {
                if (lib.get(j).year > lib.get(j + 1).year)
                {
                    HomeLibrary tmp = lib.get(j);
                    lib.set (j, lib.get(j + 1));
                    lib.set(j + 1, tmp);
                }
            }
        }

        printAll(lib);
    } //sort by date

    static void sortAuthor (ArrayList<HomeLibrary> lib) {
        for (int i = 0; i < lib.size(); i++) {
            for (int j = 0; j < lib.size() - 1; j++) {
                if (lib.get(j).author.compareTo(lib.get(j + 1).author) > 0)
                {
                    HomeLibrary tmp = lib.get(j);
                    lib.set (j, lib.get(j + 1));
                    lib.set(j + 1, tmp);
                }
            }
        }
        printAll(lib);
    }  //sort by author

    static void run () throws IOException {
        ArrayList <HomeLibrary> library = initLib();
        System.out.println("\n\u001B[1m\033[35mHOME LIBRARY\u001B[0m\033[m");
        //printAll(library);
        Scanner scan = new Scanner(System.in);
        while (true)
        {
            System.out.println("\n\u001B[1m\033[36mWhat do u want to do?\u001B[0m\033[m " +
                    "\n\033[30mp\033[m  - print all books " +
                    "\n\033[32ma\033[m  - add a new book " +
                    "\n\033[31md\033[m  - delete a book or an author " +
                    "\n\033[33mf\033[m  - find a book by a name or an author " +
                    "\n\033[34my\033[m  - find a book by year " +
                    "\n\033[36msy\033[m - sort books by year " +
                    "\n\033[30msa\033[m - sort books by authors" +
                    "\n\033[35mq\033[m  - quit");
            String ans = scan.nextLine();

            if (ans.equalsIgnoreCase("q")) {
                System.out.println("\033[31mGOOD BYE\033[m");
                break;
            }

            else if (ans.equalsIgnoreCase("p"))
                printAll(library);

            else if (ans.equalsIgnoreCase("a"))
                addBook(library);

            else if (ans.equalsIgnoreCase("d"))
                delBook(library);

            else if (ans.equalsIgnoreCase("f"))
                findAuthor(library);

            else if (ans.equalsIgnoreCase("y"))
                findYear(library);

            else if (ans.equalsIgnoreCase("sy"))
                sortYear(library);

            else if (ans.equalsIgnoreCase("sa"))
                sortAuthor(library);

            else
                System.out.println("\033[31mMISSING PARAMETER\033[m");
        }
    }
}
