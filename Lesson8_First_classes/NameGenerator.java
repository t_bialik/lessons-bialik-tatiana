package Lesson8_First_classes;

import java.util.Random;

public class NameGenerator {

    String [] partsOfName = {"ma", "na", "pa", "la", "ta", "ke", "le", "pe", "he", "o", "a", "i", "je", "ja", "jo", "ki", "di", "pi", "mi", "ni"};

    public String generateName () {

        Random random = new Random();
        String res = "";
        res += (char)(random.nextInt(26) + 65);

        for (int i = 0; i < random.nextInt(5) + 1; i++) {
            res += partsOfName[random.nextInt(partsOfName.length)];
        }

        return res;
    }
}
