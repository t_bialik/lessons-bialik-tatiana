package Lesson8_First_classes;

public class Human {

    String name;
    String surname;
    double age;
    boolean female;

    void printData ()
    {
        String sex;

        if (female == true)
            sex = "female";
        else
            sex = "male";

        System.out.printf("Name: %s \nSurname: %s \nAge: %f \nSex: %s", name, surname, age, sex);
    }
}
