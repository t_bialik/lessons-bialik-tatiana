package Lesson8_First_classes;

public class NewClass {

    int n;
    int m;

    NewClass ()
    {
        n = 0;
        m = 0;
    }

    NewClass (int n, int m)
    {
        this.n = n;
        this.m = m;
    }

    void print ()
    {
        System.out.println(n + ", " + m);
    }

    void change (int n, int m)
    {
        this.n = n;
        this.m = m;

    }

    int sum ()
    {
        return n + m;
    }

    int max ()
    {
        if (n > m)
            return n;
        else
            return m;
    }

}
