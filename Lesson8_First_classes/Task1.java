package Lesson8_First_classes;

import java.io.IOException;

public class Task1 {
    public static void main(String[] args) throws IOException {
//         Human human = new Human ();
//         human.name = "Лидия";
//         human.surname = "Иванова";
//         human.age = 54.0;
//         human.female = true;
//         human.printData();

//         NewClass myObject = new NewClass();
//         NewClass myNewObject = new NewClass (5, 7);
//         myObject.change(6, 3);
//         myNewObject.print();

//        Monster andrey = new Monster("Andrey", 100, 18, 30);
//        Monster maxim = new Monster("Maxim", 100, 25, 30);
//        andrey.info();
//        maxim.info();

        Student.run();
        HomeLibrary.run();

    }
}
