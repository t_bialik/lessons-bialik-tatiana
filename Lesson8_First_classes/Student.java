package Lesson8_First_classes;

import java.io.IOException;
import java.util.*;

public class Student {
    String name;
    int group;
    int[] marks = new int[5];

    Student() {
        name = "undefined";
        group = 0;
        Random rand = new Random();
        for (int i = 0; i < marks.length; i++) {
            marks[i] = 0;
        }
    }

    Student(String name) {
        this.name = name;
    }

    public Student(String name, int group, int[] marks) {
        this.name = name;
        this.group = group;
        this.marks = marks;
    }

    double middle() {
        double sum = 0.0;
        for (int i = 0; i < marks.length; i++) {
            sum += (double) marks[i];
        }

        double middle = sum / marks.length;
        return middle;
    }  //find a grade point

    void print() {
        System.out.println("\033[30m___\033[m\033[31m___\033[m\033[32m___\033[m\033[33m___\033[m\033[34m___\033[m\033[35m___\033[m");
        System.out.printf("\033[32mName:       \033[m %s \n\033[31mGroup:      \033[m %d \n\033[33mMarks:      \033[m %s \n\033[30mGrade point:\033[m %.2f\n", name, group, Arrays.toString(marks), middle());
        System.out.println("\033[36m------------------\033[m");
    } //print student data

    static void printSmart(ArrayList<Student> base) {
        boolean flagSmart = false;
        System.out.println("\n\u001B[1m\033[34m" + "STUDY AT 4 OR 5" + "\u001B[0m\033[m");
        for (Student i : base) {
            boolean flag = true;
            for (int j : i.marks)
                if (j < 4) {
                    flag = false;
                    break;
                }
            if (flag) {
                flagSmart = true;
                i.print();
                System.out.println();
            }
        }

        if (!flagSmart)
            System.out.println("\033[32mTHERE AREN'T SUCH STUDENTS:(\033[m");
    } //print student who study at 4 or 5

    void fillMarks(int... array) {
        this.marks = array;
    } //fill marks yourself

    void fillMarksRandom() {
        Random rand = new Random();
        for (int i = 0; i < marks.length; i++) {
            this.marks[i] = rand.nextInt(4) + 2;
        }
    } //fill marks with random

    static void sortPoints(ArrayList<Student> array) {
        Student tmp;
        for (int i = 0; i < array.size(); i++) {
            for (int j = 0; j < array.size() - 1; j++) {
                if (array.get(j).middle() > array.get(j + 1).middle()) {
                    tmp = array.get(j);
                    array.set(j, array.get(j + 1));
                    array.set(j + 1, tmp);
                }
            }
        }
    } //sort for a grade point

    static ArrayList<Student> studArray() {
        ArrayList<Student> studBase = new ArrayList<>();
        Scanner scan = new Scanner(System.in);
        //String ans = scan.nextLine();
        int count = 0;
        Random rand = new Random();

        while (true) {
            System.out.println("Write next student name");
            String ans = scan.nextLine();
            if (ans.equals("exit")) {
                break;
            } else {
                studBase.add(new Student());
                studBase.get(count).name = ans;
                studBase.get(count).fillMarksRandom();
                studBase.get(count).group = rand.nextInt(4) + 1;
                count++;
            }
        }
        return studBase;
    } //fill the base from the console

    static void printArray(ArrayList<Student> array) {
        System.out.println("\u001B[1m\033[35mSTUDENTS LIST\033[m\u001B[0m");
        for (Student i : array)
            i.print();
    } //print a students array

    static ArrayList<Student> fillFromFile(int size) throws IOException {
        ArrayList<Student> studBase = new ArrayList<>();
        Random rand = new Random();
        int count = 0;

        for (int i = 0; i < size; i++) {
            List<String> baseName = ReadFileBase.reader("src\\Lesson8_First_classes\\students_names.txt");
            List<String> base = ReadFileBase.reader("src\\Lesson8_First_classes\\surnames.txt");
            int numb = rand.nextInt(baseName.size());
            int numb_1 = rand.nextInt(base.size());

            studBase.add(new Student());
            studBase.get(count).name = baseName.get(numb) + " " + base.get(numb_1);
            studBase.get(count).fillMarksRandom();
            studBase.get(count).group = rand.nextInt(4) + 1;
            count++;
        }

        return studBase;
    } //fill the base from files

    static void run () throws IOException {
        ArrayList <Student> students = fillFromFile(10);

        System.out.println("\u001B[1m\033[35mHELLO! It's a students database\u001B[0m\033[m ");

        Scanner scan = new Scanner(System.in);
        while (true) {
            System.out.println("\n\u001B[1m\033[36mWhat do u want to do?\u001B[0m\033[m " +
                    "\n\033[30mp\033[m - print students list " +
                    "\n\033[32mg\033[m - print students who study at 4 or 5 " +
                    "\n\033[31ms\033[m - sort students list for a grade point " +
                    "\n\033[35mq\033[m - quit");
            String ans = scan.nextLine();

            if (ans.equalsIgnoreCase("q")) {
                System.out.println("\033[31mGOOD BYE\033[m");
                break;
            }

            else if (ans.equalsIgnoreCase("p")) {
                printArray(students);
            }

            else if (ans.equalsIgnoreCase("s"))
            {
                sortPoints(students);
                printArray(students);
            }

            else if (ans.equalsIgnoreCase("g"))
                printSmart(students);

            else
                System.out.println("\033[31mMISSING PARAMETER\033[m");
        }
    } //a main method
}
