package Lesson8_First_classes;

import java.util.Scanner;

public class LibWithoutCheats {

    MyBook [] library;
    int size;

    LibWithoutCheats (int size) {

        library = new MyBook[size];
        this.size = size;

        for (int i = 0; i < size; i++) {
            library [i] = new MyBook();
        }
    }

    void printLib () {
        for (int i = 0; i < size; i++) {
            library[i].print();
        }
    }

    void addBook () {
        if (library.length == size) {
            MyBook [] tmp = new MyBook [library.length * 2];

            for (int i = 0; i < library.length; i++) {
                tmp [i] = library [i];
            }

            library = tmp;
        }

        Scanner scan = new Scanner(System.in);
        System.out.println("Введите название книги");
        String name = scan.nextLine();
        System.out.println("Введите автора");
        String author = scan.nextLine();
        System.out.println("Введите год издания");
        int year = scan.nextInt();

        library [size].name = name;
        library [size].author = author;
        library [size].year = year;

        this.size++;
    }

    void delBook () {

    }

    void sortByAuthor () {

    }

    void sortByYear () {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size - 1; j++) {
                if (library[j].year > library[j + 1].year) {
                    MyBook tmp = library [j];
                    library [j] = library[j + 1];
                    library[j + 1] = tmp;
                }
            }
        }
    }

}
