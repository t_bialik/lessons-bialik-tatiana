package Lesson7_1D2D3D_Arrays;

import java.util.Random;
import java.util.Scanner;

public class Arrays1D {

    static int [] fill1D ()
    {
        System.out.println("Введите размерность массива");
        Scanner scan = new Scanner (System.in);
        int n = scan.nextInt();
        Random rand = new Random();
        int [] arrayNew = new int [n];

        for (int i = 0; i < n; i++) {
            arrayNew[i] = rand.nextInt(10) - 3;
        }
        return arrayNew;
    }

    static void print1D (int [] array)
    {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }
}
