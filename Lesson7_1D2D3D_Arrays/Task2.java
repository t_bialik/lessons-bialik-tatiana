package Lesson7_1D2D3D_Arrays;

public class Task2 {
    public static void main(String[] args) {

        int [] array = Arrays1D.fill1D();
        Arrays1D.print1D(array);

        //Задачи 2 и 3
        int max = array [0];
        int min = array [0];

        for (int i = 0; i < array.length; i++) {
            if (array [i] > max)
                max = array [i];
        }

        for (int i = 0; i < array.length; i++) {
            if (array [i] < min)
                min = array [i];
        }

        System.out.println("Максимальное значение массива: " + max);
        System.out.println("Минимальное значение массива: " + min);

        //Задача 4
        int index = -1;
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            if (array [i] == 1)
            {
                index = i;
                break;
            }
        }

        if (index != -1)
        {
            for (int i = index + 1; i < array.length; i++) {
                sum +=array [i];
            }
            System.out.println("Сумма элементов после 1: " + sum);
        }
        else
            System.out.println(0);
    }
}
