package Lesson7_1D2D3D_Arrays;

public class Task25 {
    public static void main(String[] args) {
        int [][][] array = ArraysFunc.fill3D();
        ArraysFunc.print3D(array);

        System.out.println("Task 25 \nСумма элементов трехмерного массива " + summ (array));

        changeSign(array);
        System.out.println("\nTask 26 \nМассив с измененными знаками");
        ArraysFunc.print3D(array);

        System.out.println("\nTask 27 \nСреднее арифметическое неотрицательных элементов " + midAr(array));

//        sortArray(array);
//        System.out.println("\nTask 28 \nОтсортированный массив");
//        ArraysFunc.print3D(array);
//
//        System.out.println("\nTask 29 \nОтсортированный массив");
//        sortSum(array);
//        ArraysFunc.print3D(array);

        System.out.println("\nTask 30 \nМассив, преобразованный в строку");
        Arrays1D.print1D(arraytoString(array));

        System.out.println("\nTask 31 \nПлоскость, содержащая максимум");
        ArraysFunc.printArray(findMaxXY(array));

        System.out.println("\nTask 32 \nМассив");
        print3D(array);

        System.out.println("\nTask 33 \nСредние арифметические плоскостей");
        printMidAr(array);

        System.out.println("\nTask 34 \nСортировка по среднему арифметическому");
        sortMid(array);
        ArraysFunc.print3D(array);
    }

    static int summ (int [][][] array)  //сумма всех элементов 3д массива
    {
        int summa = 0;

        for (int [][] array2D : array) {
            for (int [] array1D : array2D) {
                for (int i : array1D)
                    summa += i;
            }
        }
        return summa;
    }

    static void changeSign (int [][][] array)  //замена знаков в отрицательных элементах
    {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                for (int k = 0; k < array[0][0].length; k++) {
                    if (array [i][j][k] < 0)
                        array [i][j][k] = -array [i][j][k];
                }
            }
        }
    }

    static double midAr (int [][][] array)  //среднее арифметическое неотрицательных элементов
    {
        double summa = 0;
        double count = 0.0;

        for (int [][] array2D : array) {
            for (int [] array1D : array2D) {
                for (int i : array1D)
                    if (i >= 0)
                    {
                        summa += (double)i;
                        count++;
                    }
            }
        }
        return summa / count;
    }

    static void sortArray (int [][][] array)   //сортировка 3д массива
    {
        int tmp = 0;

        for (int i = 0; i < array.length * array [0].length * array [0][0].length; i++) {
            for (int j = 0; j < array.length; j++) {
                for (int k = 0; k < array[0].length; k++) {
                    for (int l = 0; l < array[0][0].length - 1; l++) {
                        if (array [j][k][l] > array [j][k][l + 1])
                        {
                            tmp = array[j][k][l];
                            array [j][k][l] = array [j][k][l + 1];
                            array [j][k][l + 1] = tmp;
                        }
                    }

                    if (k != array[0].length - 1 && array [j][k][array[0][0].length - 1] > array [j][k + 1][0])
                    {
                        tmp = array [j][k][array[0][0].length - 1];
                        array [j][k][array[0][0].length - 1] = array [j][k + 1][0];
                        array [j][k + 1][0] = tmp;
                    }
                }

                if (j != array.length - 1 && array [j][array[0].length - 1][array[0][0].length - 1] > array [j + 1][0][0])
                {
                    tmp = array [j][array[0].length - 1][array[0][0].length - 1];
                    array [j][array[0].length - 1][array[0][0].length - 1] = array [j + 1][0][0];
                    array [j + 1][0][0] = tmp;
                }
            }
        }
    }

    static void sortSum (int [][][] array)   //сортировка плоскостей по сумме элементов в них
    {
        for (int i = 0; i < array.length; i++) {
            System.out.print(sum (array[i]) + "\t");
        }
        System.out.println("\n");

        int [][] tmp;
        for (int i = 0; i < array.length * array [0].length * array [0][0].length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (sum(array[j]) > sum(array[j + 1]))
                {
                    tmp = array [j];
                    array [j] = array [j + 1];
                    array [j + 1] = tmp;
                }
            }
        }
    }

    static int [] arraytoString (int [][][] array)  //преобразование 3д массива в строку
    {
        int [] str = new int [array.length * array[0].length * array[0][0].length];
        int count = 0;
        for (int [][] array2D : array) {
            for (int [] array1D : array2D) {
                for (int i : array1D) {
                    str [count] = i;
                    count++;
                }
            }
        }

        return str;
    }

    static int [][] findMaxXY (int [][][] array)  //нахождение плоскости, содержащей максимум
    {
        int [][] result = new int [array[0].length][array[0][0].length];
        for (int [][] i : array)
        {
            if (findElem (i, findMax (array)))
                result = i;
        }
        return result;
    }

    static int findMax (int [][][] array)   //нахождение максимума в 3д массиве
    {
        int max = array [0][0][0];

        for (int [][] i : array) {
            for (int [] j : i) {
                for (int k : j)
                    if (k > max)
                        max = k;
            }
        }

        return max;
    }

    static boolean findElem (int [][] array, int elem)   //проверка, содержится ли заданный элемент в данном 2д массиве
    {
        boolean flag = false;

        for (int [] i : array)
        {
            for (int j : i)
            {
                if (j == elem)
                {
                    flag = true;
                    break;
                }
            }
        }

        return flag;
    }

    static void print3D (int [][][] array)   //вывод 3д массива с номером плоскости
    {
        int z = 0;
        for (int [][] array2d : array) {
            System.out.println("z = " + z);
            for (int [] array1d : array2d) {
                for (int i: array1d) {
                    System.out.print(i + "\t");
                }
                System.out.println();
            }
            System.out.println("");
            z++;
        }
    }

    static int sum (int [][] array)  //сумма всех элементов в 2д массиве
    {
        int summa = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                summa += array[i][j];
            }
        }
        return summa;
    }

    static double midAr (int [][] array)  //среднее арифметическое 2д массива
    {
        double m = (double)sum (array) / (array.length * array[0].length);
        return m;
    }

    static void printMidAr (int [][][] array)  //вывод средних арифметических всех плоскостей
    {
        for (int i = 0; i < array.length - 1; i++)
            System.out.print(midAr(array[i]) + ", ");
        System.out.println(midAr(array[array.length - 1]));
    }

    static void sortMid (int [][][] array)   //сортировка 3д массива по среднему арифметическому плоскостей
    {
        int [][] tmp;
        for (int i = 0; i < array.length * array [0].length * array [0][0].length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (midAr(array[j]) > midAr(array[j + 1]))
                {
                    tmp = array [j];
                    array [j] = array [j + 1];
                    array [j + 1] = tmp;
                }
            }
        }
    }

}
