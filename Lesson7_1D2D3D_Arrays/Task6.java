package Lesson7_1D2D3D_Arrays;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите размерности массива");
        int n = scan.nextInt();
        int m = scan.nextInt();
        int k = scan.nextInt();
        int [][][] array = new int [n][m][k];

        fillArray(array);
        print3D(array);
    }

    static void fillArray (int [][][] array)
    {
        int i = 0;
        int j = 0;
        int k = 0;

        while (i < array.length)
        {
            while (j < array[0].length)
            {
                while (k < array[0][0].length)
                {
                    array [i][j][k] = 1;
                    k++;
                }
                j++;
                k = 0;
            }
            j = 0;
            i++;
        }
    }

    static void print3D (int [][][] array)
    {
        for (int [][] array2d : array) {
            for (int [] array1d : array2d) {
                for (int i: array1d) {
                    System.out.print(i + " ");
                }
                System.out.print("\t");
            }
            System.out.println();
        }
    }
}
