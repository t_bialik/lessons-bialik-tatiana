package Lesson7_1D2D3D_Arrays;

public class Task7 {
    public static void main(String[] args) {
        int [] array = Arrays1D.fill1D();
        int [] array_1 = {1,1,1,1,1,1,1};

        Arrays1D.print1D(array);
        check (array);

        Arrays1D.print1D(array_1);
        check (array_1);
    }

    static void check (int [] array)
    {
        boolean flag = true;
        for (int i = 0; i < array.length - 1; i++) {
            if (array [i] != array [i + 1])
                flag = false;
        }

        if (flag)
            System.out.println("true");
        else
            System.out.println("false");
    }
}
