package Lesson7_1D2D3D_Arrays;

public class Task5 {
    public static void main(String[] args) {

        int [][] array = ArraysFunc.fillArray();
        ArraysFunc.printArray(array);

        int max = array[0][0];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                if (i == j)
                    if (array [i][j] > max)
                        max = array [i][j];
            }
        }
        System.out.println("Максимальный элемент на главной диагонали матрицы " + max);
    }
}
