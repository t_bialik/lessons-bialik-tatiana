package Lesson7_1D2D3D_Arrays;

import java.util.Random;
import java.util.Scanner;

public class ArraysFunc {

    static int [][] fillArray ()
    {
        System.out.println("Введите размерность массива");
        Scanner scan = new Scanner (System.in);
        int n = scan.nextInt();
        int m = scan.nextInt();
        int [][] array = new int [n][m];
        Random rand = new Random();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = rand.nextInt(99) - 50;
            }
        }

        return array;
    }

    static void printArray (int [][] array)
    {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length - 1; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println(array[i][array[0].length - 1]);
        }
        System.out.println("");
    }

    static void printDoubleArray (double [][] array)
    {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length - 1; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println(array[i][array[0].length - 1]);
        }
        System.out.println("");
    }

    static int [][][] fill3D ()
    {
        System.out.println("Введите размерность трехмерного массива");
        Scanner scan = new Scanner (System.in);
        int n = scan.nextInt();
        int m = scan.nextInt();
        int k = scan.nextInt();
        int [][][] array = new int [n][m][k];
        Random rand = new Random();

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                for (int l = 0; l < array[0][0].length; l++) {
                    array [i][j][l] = rand.nextInt(100) - 40;
                }
            }
        }
        return array;
    }

    static void print3D (int [][][] array)
    {
        for (int [][] array2d : array) {
            for (int [] array1d : array2d) {
                for (int i: array1d) {
                    System.out.print(i + "\t");
                }
                System.out.println();
            }
            System.out.println("");
        }
    }
}
