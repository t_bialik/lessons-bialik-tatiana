package Lesson7_1D2D3D_Arrays;

import java.util.Arrays;

public class Task8 {
    public static void main(String[] args) {
        int [] array = Arrays1D.fill1D();
        Arrays1D.print1D(array);

        System.out.println("Task 8");
        System.out.println("Сумма положительных элементов массива " + summa (array));

        boolean flag = checkUp(array);
        System.out.println("\nTask 9");
        if (flag)
            System.out.println("Элементы расположены по возрастанию");
        else
            System.out.println("Элементы не расположены по возрастанию");

        System.out.println("\nTask 10");
        System.out.print("Три максимальных элемента массива: ");
        Arrays1D.print1D(find3Max(array));

        System.out.println("\nTask 11");
        turnArray(array);
        System.out.print("Перевернутый массив: ");
        Arrays1D.print1D(array);

        System.out.println("\nTask 12");
        circle (array);
        Arrays1D.print1D(array);

        System.out.println("\nTask 13-14");
        System.out.println("Максимальный элемент массива " + findMax(array) + ", а минимальный - " + findMin(array));

        System.out.println("\nTask 15");
        System.out.print("Все элементы массива различны: ");
        difference (array);

        System.out.println("\nTask 16");
        System.out.println("Количество различных элементов в массиве " + diffNumb(array));

        System.out.println("\nTask 17");
        System.out.println("Количество 0 в массиве " + countZero (array));

        int k = 5;
        System.out.println("\nTask 18");
        System.out.println("Количество элементов, меньших " + k + ": " + countMin(array, k));

        System.out.println("\nTask 19");
        System.out.println("Нормированный массив");
        for (double i: normArray(array))
            System.out.print(i + " ");
        System.out.println();

    }

    static int summa (int [] array)  //сумма элементов одномерного массива
    {
        int sum = 0;
        for (int i : array) {
            if (i > 0)
                sum += i;
        }
        return sum;
    }

    static boolean checkUp (int [] array)  //проверка, по возрастанию ли расположены элементы
    {
        boolean flag = true;

        for (int i = 0; i < array.length - 1; i++) {
            if (array [i] > array [i + 1])
                flag = false;
        }
        return flag;
    }

    static int [] find3Max (int [] array)  //поиск трех максимальных элементов
    {
        int [] maxArray = new int [3];
        maxArray[0] = array [0];
        maxArray[1] = array [1];

        for (int i = 0; i < array.length; i++) {
            if (array [i] > maxArray[0])
                maxArray[0] = array [i];
        }

        for (int i = 1; i < 3; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array[j] < maxArray [i - 1] && array [j] > maxArray [i])
                    maxArray [i] = array [j];
            }
        }
        return maxArray;
    }

    static void turnArray (int [] array)   //расположение элементов в обратном порядке
    {
        for (int i = 0; i < array.length / 2; i++) {
            int tmp = array [i];
            array [i] = array [array.length - i - 1];
            array [array.length - 1 - i] = tmp;
        }
    }

    static void circle (int [] array)   //циклический сдвиг вправо на 1
    {
        int last = array [array.length - 1];

        for (int i = array.length - 1; i > 0; i--)
            array [i] = array [i - 1];

        array [0] = last;
    }

    static int findMax (int [] array)  //поиск максимума
    {
        int max = array [0];

        for (int i = 0; i < array.length; i++) {
            if (array[i] > max)
                max = array[i];
        }
        return max;
    }

    static int findMin (int [] array)  //поиск минимума
    {
        int min = array [0];

        for (int i = 0; i < array.length; i++) {
            if (array[i] < min)
                min = array[i];
        }
        return min;
    }

    static void difference (int [] array)  //проверка, все ли элементы массива различны
    {
        boolean flag = true;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array [i] == array [j])
                {
                    System.out.println("false");
                    flag = false;
                    break;
                }
            }
            break;
        }
        if (flag)
            System.out.println("true");
    }

    static int diffNumb (int [] array)   //счетчик количества различных элементов
    {
        int count = array.length;

        Arrays.sort (array);

        for (int i = 0; i < array.length - 1; i++) {
            if (array [i] == array [i + 1])
                count--;

        }

        return count;
    }

    static int countZero (int [] array)   //счетчик нулей в массиве
    {
        int count = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0)
                count++;
        }
        return count;
    }

    static int countMin (int [] array, int k)  //счетчик чисел, меньше заданного k
    {
        int count = 0;
//        Scanner scan = new Scanner(System.in);
//        System.out.println("Введите число для поиска элементов, меньше него");
//        int k = scan.nextInt();

        for (int i = 0; i < array.length; i++) {
            if (array[i] < k)
                count++;
        }
        return count;
    }

    static double [] normArray (int [] array)  //нормирование неотрицательных чисел относительно их суммы
    {
        double sum = 0;
        double [] array_1 = new double [array.length];

        for (int i = 0; i < array.length; i++) {
            if (array [i] >= 0)
                sum += array [i];
        }

        for (int i = 0; i < array.length; i++) {
            if (array[i] >= 0)
                array_1 [i] = array[i] / sum;
            else
                array_1 [i] = array [i];
        }

        return array_1;
    }
}
