package Lesson7_1D2D3D_Arrays;

public class Task20 {
    public static void main(String[] args) {
        int [][] array = ArraysFunc.fillArray();
        ArraysFunc.printArray(array);

        System.out.println("Task 20 \nСумма всех элементов массива равна " + sum (array));

        System.out.println("Task 21 \nСреднее арифметическое всех элементов " + midAr (array));

        System.out.println("Task 22 \nСумма элементов на главной и побочной диагоналях " + sumDiag(array));

        System.out.println("Task 23 \nНормированный массив");
        ArraysFunc.printDoubleArray(normMax(array));

        System.out.println("Task 24 \nРезультат");
        divArray(array);
        ArraysFunc.printArray(array);
    }

    static int sum (int [][] array)
    {
        int summa = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                summa += array[i][j];
            }
        }
        return summa;
    }

    static double midAr (int [][] array)
    {
        double m = (double)sum (array) / (array.length * array[0].length);
        return m;
    }

    static int sumDiag (int [][] array)
    {
        int sum = 0;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                if (i == j)
                    sum += array [i][j];
            }
        }

        for (int i = 0; i < array.length; i++) {
            sum += array [i][array.length - i - 1];
        }

        if (array.length % 2 != 0)
            sum -= array[array.length / 2 + 1][array.length / 2 + 1];

        return sum;
    }

    static double [][] normMax(int [][] array)
    {
        double [][] array_1 = new double [array.length][array[0].length];
        double max = (double) array[0][0];

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                if (array[i][j] > max)
                    max = (double) array [i][j];
            }
        }

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                array_1[i][j] = array[i][j] / max;
            }
        }

        return array_1;
    }

    static void divArray (int [][] array)
    {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                if (Math.abs(array[i][j]) > 10)
                    array[i][j] /= 2;
            }
        }
    }
}
