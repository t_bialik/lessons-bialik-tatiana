package Lesson7_1D2D3D_Arrays;

public class Task1 {
    public static void main(String[] args) {
        char [] alphabet = new char [26];

        for (int i = 65; i < 91; i++) {
            alphabet [90 - i] = (char) i;
        }

        for (int i = 25; i >= 0; i--) {
            System.out.print(alphabet[i] + " ");
        }
    }
}
