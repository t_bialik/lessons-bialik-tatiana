package Lesson3;

public class Math3 {
    public static void main(String[] args) {

        double max = 0;
        double point = 0;

        for (double x = -10; x < 10; x+=0.01) {
            if (Math.cos(x) + Math.pow (x,2) > Math.cos(max) + Math.pow (max,2))
            {
                max = Math.cos(x) + Math.pow (x,2);
                point = x;
            }
        }
        System.out.println("Максимальное значение " + max + " достигается при х = " + point);
    }
}
