package Lesson3;

public class Math8 {
    public static void main(String[] args) {

        double max = 0;
        double point = 0;

        for (double x = -10; x <= 10; x+=0.01) {
            if (Math.pow(Math.E,x) > Math.pow(Math.E,max))
            {
                max = Math.pow(Math.E,x);
                point = x;
            }
        }
        System.out.println("Максимальное значение " + max + " достигается при х = " + point);
    }
}
