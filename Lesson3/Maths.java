package Lesson3;

public class Maths {
    public static void main(String[] args) {

        double max = 0;
        double point = 0;

        for (double x = -15; x <= 15; x+=0.01) {
            if (Math.pow(x,2) - x + 3 > Math.pow(point,2) - point + 3)
            {
                max = Math.pow(x,2) - x + 3;
                point = x;
            }
        }
        System.out.println("Максимальное значение " + max + " достигается при х = " + point);
    }
}
