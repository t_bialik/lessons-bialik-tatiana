package Lesson3;

public class Math5 {
    public static void main(String[] args) {

        double point = -50;
        double max = Math.pow (point,3);

        for (double x = -50; x < 50; x+=0.01) {
            if (Math.pow (x,3) > Math.pow (point,3))
            {
                max = Math.pow (x,3);
                point = x;
            }
        }
        System.out.println("Максимальное значение " + max + " достигается при х = " + point);
    }
}
