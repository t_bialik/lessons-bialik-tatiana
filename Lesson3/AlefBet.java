package Lesson3;

import java.util.Scanner;

public class AlefBet {
    public static void main(String[] args) {

        Scanner in = new Scanner (System.in);
        String s = "";

        System.out.println("Введите строку");

        String words = in.nextLine();
        String [] wordArray = words.split(" ");

        //Arrays.sort (wordArray);

        for (int i = 1; i < wordArray.length - 1; i++) {
            for (int j = 0; j < wordArray.length - i; j++) {
                if (wordArray[j].compareTo(wordArray[j + 1]) > 0)
                {
                    s = wordArray[j];
                    wordArray[j] = wordArray[j + 1];
                    wordArray[j + 1] = s;
                }
            }
        }

        for (String i : wordArray) {
            System.out.println(i);
        }
    }
}
