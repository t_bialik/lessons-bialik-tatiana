package Lesson3;

public class Math2 {
    public static void main(String[] args) {

        double max = 0;
        double point = 0;

        for (double x = -10; x < 10; x+=0.01) {
            if (Math.sin(x) + Math.pow (x,2) > Math.sin(max) + Math.pow (max,2))
            {
                max = Math.sin(x) + Math.pow (x,2);
                point = x;
            }
        }
        System.out.println("Максимальное значение " + max + " достигается при х = " + point);
    }
}
