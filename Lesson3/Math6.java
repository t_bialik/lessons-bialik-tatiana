package Lesson3;

public class Math6 {
    public static void main(String[] args) {

        double point = -10;
        double max = 1/Math.pow (point,3);

        for (double x = -10; x < 10; x+=0.01) {
            if (!(x > -1 && x < 1))
            {
                if (Math.pow (x,3) > Math.pow (point,3))
                {
                    max = Math.pow (x,3);
                    point = x;
                }
            }
        }
        System.out.println("Максимальное значение " + max + " достигается при х = " + point);
    }
}
