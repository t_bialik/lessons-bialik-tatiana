package Lesson12_Enum.Task1;

public enum DaysOfWeek {

    MONDAY (8),
    TUESDAY (8),
    WEDNESDAY (8),
    THURSDAY (8),
    FRIDAY (7),
    SATURDAY (0),
    SUNDAY (0);

    private int numHours;

    DaysOfWeek(int numHours) {
        this.numHours = numHours;
    }

    int getNumHours () {
        return numHours;
    }

    int getWorkingHours () {
        int hours = 0;
        DaysOfWeek [] days = DaysOfWeek.values();

        for (int i = this.ordinal(); i < days.length; i++) {
            hours += days[i].numHours;
        }

        return hours;
    }
}
