package Lesson12_Enum.Task1;

public class Main {
    public static void main(String[] args) {
        System.out.printf("From Monday to the end of week is %d working hours and from Wednesday - %d hours",
                DaysOfWeek.MONDAY.getWorkingHours(), DaysOfWeek.WEDNESDAY.getWorkingHours());
    }
}
