package Lesson12_Enum.Task2;

import java.util.Scanner;

public class BookOfMeetings {

    void createBook(DateOfMeet[] book) {
        //DateOfMeet [] book = new DateOfMeet[365];

        int countDays = 1;

        for (int i = 0; i < book.length; i++) {
            book[i] = new DateOfMeet();
            book[i].hour = -1;

            if (i < 31) {
                book[i].num = countDays;
                book[i].month = Months.JANUARY.name;
                if (countDays == Months.JANUARY.days)
                    countDays = 0;
            }
            if (i > 30 && i < 59) {
                book[i].num = countDays;
                book[i].month = Months.FEBRUARY.name;
                if (countDays == Months.FEBRUARY.days)
                    countDays = 0;
            }
            if (i > 58 && i < 90) {
                book[i].num = countDays;
                book[i].month = Months.MARCH.name;
                if (countDays == Months.MARCH.days)
                    countDays = 0;
            }
            if (i > 89 && i < 120) {
                book[i].num = countDays;
                book[i].month = Months.APRIL.name;
                if (countDays == Months.APRIL.days)
                    countDays = 0;
            }
            if (i > 119 && i < 151) {
                book[i].num = countDays;
                book[i].month = Months.MAY.name;
                if (countDays == Months.MAY.days)
                    countDays = 0;
            }
            if (i > 150 && i < 181) {
                book[i].num = countDays;
                book[i].month = Months.JUNE.name;
                if (countDays == Months.JUNE.days)
                    countDays = 0;
            }
            if (i > 180 && i < 212) {
                book[i].num = countDays;
                book[i].month = Months.JULY.name;
                if (countDays == Months.JULY.days)
                    countDays = 0;
            }
            if (i > 211 && i < 243) {
                book[i].num = countDays;
                book[i].month = Months.AUGUST.name;
                if (countDays == Months.AUGUST.days)
                    countDays = 0;
            }
            if (i > 242 && i < 273) {
                book[i].num = countDays;
                book[i].month = Months.SEPTEMBER.name;
                if (countDays == Months.SEPTEMBER.days)
                    countDays = 0;
            }
            if (i > 272 && i < 304) {
                book[i].num = countDays;
                book[i].month = Months.OCTOBER.name;
                if (countDays == Months.OCTOBER.days)
                    countDays = 0;
            }
            if (i > 303 && i < 334) {
                book[i].num = countDays;
                book[i].month = Months.NOVEMBER.name;
                if (countDays == Months.NOVEMBER.days)
                    countDays = 0;
            }
            if (i > 333) {
                book[i].num = countDays;
                book[i].month = Months.DECEMBER.name;
                if (countDays == Months.DECEMBER.days)
                    countDays = 0;
            }

            countDays++;
        }

        for (int i = 0; i < book.length - 1; i += 7) {
            book[i].day = DaysOfWeek.MONDAY.name;
            book[i + 1].day = DaysOfWeek.TUESDAY.name;
            book[i + 2].day = DaysOfWeek.WEDNESDAY.name;
            book[i + 3].day = DaysOfWeek.THURSDAY.name;
            book[i + 4].day = DaysOfWeek.FRIDAY.name;
            book[i + 5].day = DaysOfWeek.SATURDAY.name;
            book[i + 6].day = DaysOfWeek.SUNDAY.name;
        }
        book[book.length - 1].day = DaysOfWeek.MONDAY.name;
    }

    void displayBook(DateOfMeet[] book) {
        for (int i = 0; i < book.length; i++) {
            book[i].show();
        }
    }

    void addMeeting(DateOfMeet[] book) {
        DateOfMeet myMeet = DateOfMeet.fillInfo();

        for (int i = 0; i < book.length; i++) {

            if (book[i].month.equals(myMeet.month) && book[i].num == myMeet.num)
                if (book[i].hour != -1) {
                    System.out.println("\nERROR!\nYou have a meeting on this day");
                    book[i].show();
                }
                else {
                    book[i].hour = myMeet.hour;
                    book[i].meetName = myMeet.meetName;
                    book[i].meetInfo = myMeet.meetInfo;
                    book[i].minute = myMeet.minute;
                }
        }
    }

    void showMeetings(DateOfMeet[] book) {
        for (int i = 0; i < book.length; i++) {
            if (book[i].hour != -1)
                book[i].show();
        }
    }

    void run() {
        DateOfMeet[] bookOfMeetings = new DateOfMeet[365];
        createBook(bookOfMeetings);
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.printf("Choose an action: \n1 - show meetings \n2 - add a meeting \n3 - show all book\n");
            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    showMeetings(bookOfMeetings);
                    break;
                case 2:
                    addMeeting(bookOfMeetings);
                    break;
                case 3:
                    displayBook(bookOfMeetings);
                    break;
            }

            System.out.println("Next action? \"n\" to exit");
            String exit = scanner.next();

            if (exit.equals("n"))
                break;
        }
    }
}
