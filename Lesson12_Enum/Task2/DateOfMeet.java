package Lesson12_Enum.Task2;

import java.util.Scanner;

public class DateOfMeet {

    String day;
    String month;
    String meetName;
    String meetInfo;
    int hour;
    int minute;
    int num;

    DateOfMeet() {

    }

    public DateOfMeet(String day, String month, String meetName, String meetInfo, int hour, int minute, int num) {
        this.day = day;
        this.month = month;
        this.meetName = meetName;
        this.meetInfo = meetInfo;
        this.hour = hour;
        this.minute = minute;
        this.num = num;
    }

    void show() {
        System.out.println("You have a " + meetName + " at " + hour + ":" + minute + " on " + num + " " + month + ", " + day + ". \nInfo: " + meetInfo + "\n");
    }

    static DateOfMeet fillInfo() {
        String tmp;
        DateOfMeet myMeeting = new DateOfMeet();
        Scanner scanner = new Scanner(System.in);

//        System.out.println("Input a week day of meet");
//        tmp = scanner.next().toUpperCase();
//        myMeeting.day = DaysOfWeek.valueOf(tmp).name;

        System.out.println("Input a month");
        tmp = scanner.next().toUpperCase();
        myMeeting.month = Months.valueOf(tmp).name;

        System.out.println("Input a day");
        int tmpInt = scanner.nextInt();
        myMeeting.num = tmpInt;

        System.out.println("Input name of meet");
        myMeeting.meetName = scanner.next();

        System.out.println("Input time: HH MM");
        myMeeting.hour = scanner.nextInt();
        myMeeting.minute = scanner.nextInt();

        System.out.println("Input meet info");
        myMeeting.meetInfo = scanner.next();

        return myMeeting;
    }
}
