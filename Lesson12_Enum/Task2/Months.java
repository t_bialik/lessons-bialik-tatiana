package Lesson12_Enum.Task2;

public enum Months {

    JANUARY ("January", 31),
    FEBRUARY ("February", 28),
    MARCH ("March", 31),
    APRIL ("April", 30),
    MAY ("May", 31),
    JUNE ("June", 30),
    JULY ("July", 31),
    AUGUST ("August", 31),
    SEPTEMBER ("September", 30),
    OCTOBER ("October", 31),
    NOVEMBER ("November", 30),
    DECEMBER ("December", 31);

    String name;
    int days;

    Months(String name, int days) {
        this.name = name;
        this.days = days;
    }
}
