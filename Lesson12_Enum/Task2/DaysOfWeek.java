package Lesson12_Enum.Task2;

public enum DaysOfWeek {

    MONDAY ("Monday"),
    TUESDAY ("Tuesday"),
    WEDNESDAY ("Wednesday"),
    THURSDAY ("Thursday"),
    FRIDAY ("Friday"),
    SATURDAY ("Saturday"),
    SUNDAY ("Sunday");

    String name;

    DaysOfWeek(String name) {
        this.name = name;
    }
}
