package Orcs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public abstract class Tribe {

    ArrayList <Orc> tribe;
    int max;

    public Tribe (int max) {
        this.max = max;
        this.tribe = new ArrayList<>();
        fill();
    }

    private void fill(){
        Random random = new Random();
        for (int i = 0; i < max; i++) {

            switch (random.nextInt(4)) {
                case 0:
                    tribe.add(new Farmer());
                    break;
                case 1:
                    tribe.add(new Mage());
                    break;
                case 2:
                    tribe.add(new Warrior());
                    break;
                case 3:
                    tribe.add(new Major());
                    break;
            }
        }
    }

    void printTribe () {
        System.out.println("\n\n" + this.getClass().getSimpleName().toUpperCase());
        for (int i = 0; i < tribe.size(); i++) {
            System.out.println("----------");
            System.out.println((i + 1) + " " + tribe.get(i).toString());
            System.out.println("----------");
        }
        System.out.printf("MEAL in the %s: %.1f\n", getClass().getSimpleName(), sumMeal());
        System.out.println(Arrays.toString(middleStats()));
    }

    private double sumMeal () {
        double meal = 0.0;
        for (Orc orc : tribe) {
            meal += orc.meal;
        }
        return meal;
    }

    private double [] middleStats () {
        double [] stats = {0,0,0};
        for (Orc orc : tribe) {
            stats[0] += orc.hp;
            stats[1] += orc.damage;
            stats[2] += orc.armor;
        }

        for (int i = 0; i < stats.length; i++) {
            stats[i] /= tribe.size();
        }

        return stats;
    }

}
