package Orcs;

import java.util.Random;

public class Army extends Tribe {

    public Army(int max) {
        super(max);
        getSkills();
    }

    private void getSkills () {
        Random random = new Random();
        for (int i = 0; i < max; i++) {
            this.tribe.get(i).armor += this.tribe.get(i).armor * (random.nextInt(6) + 10) / 100;
            this.tribe.get(i).hp += this.tribe.get(i).hp * (random.nextInt(6) + 10) / 100;
            this.tribe.get(i).damage += this.tribe.get(i).damage * (random.nextInt(6) + 10) / 100;
        }
    }
}
