package Orcs;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Action {

    void run() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("How many orcs in Army?");
        int maxArmy = scanner.nextInt();
        System.out.println("How many orcs in Farm?");
        int maxFarm = scanner.nextInt();
        System.out.println("How many orcs in Castle?");
        int maxCastle = scanner.nextInt();

        Tribe army = new Army(maxArmy);
        Tribe farm = new Farm(maxFarm);
        Tribe castle = new Castle(maxCastle);

        army.printTribe();
        farm.printTribe();
        castle.printTribe();
        System.out.println();

        System.out.println("Who will be fight? \n1 - castle vs army \n2 - castle vs farm \n3 - farm vs army");
        int choice = scanner.nextInt();

        switch (choice) {
            case 1:
                fight(castle, army);
                break;
            case 2:
                fight(castle, farm);
                break;
            case 3:
                fight(farm, army);
                break;
            default:
                System.out.println("No such parameter!");
        }
    }

    void fight(Tribe tribeOne, Tribe tribeTwo) {
        while (tribeOne.tribe.size() > 0 && tribeTwo.tribe.size() > 0) {
            hit(tribeOne.tribe, tribeTwo.tribe);
            checkTribe(tribeOne);
            checkTribe(tribeTwo);
        }
        if (tribeOne.tribe.size() > 0) {
            System.out.print("\n\033[31m" + tribeOne.getClass().getSimpleName().toUpperCase() + " WIN!\033[m");
            tribeOne.printTribe();
        } else if (tribeTwo.tribe.size() > 0) {
            System.out.print("\n\033[31m" + tribeTwo.getClass().getSimpleName().toUpperCase() + " WIN!\033[m");
            tribeTwo.printTribe();
        } else
            System.out.println("\033[31mEVERYBODY DEAD!\033[m");
    }

    void hit(ArrayList<Orc> tribeOne, ArrayList<Orc> tribeTwo) {
        Random random = new Random();
        for (int i = 0; i < Math.min(tribeOne.size(), tribeTwo.size()); i++) {
            System.out.println("Fighting " + tribeOne.get(i).getClass().getSimpleName() + " " + tribeOne.get(i).name + " vs "
                    + tribeTwo.get(i).getClass().getSimpleName() + " " + tribeTwo.get(i).name);
            switch (random.nextInt(2)) {
                case 0:
                    tribeOne.get(i).attack(tribeTwo.get(i));
                    if (checkAlive(tribeTwo.get(i))) {
                        tribeTwo.get(i).attack(tribeOne.get(i));
                        break;
                    } else {
                        break;
                    }

                case 1:
                    tribeTwo.get(i).attack(tribeOne.get(i));
                    if (checkAlive(tribeOne.get(i))) {
                        tribeOne.get(i).attack(tribeTwo.get(i));
                        break;
                    } else {
                        break;
                    }
            }
        }
    }

    void checkTribe(Tribe tribe) {
        for (int i = 0; i < tribe.tribe.size(); i++) {
            if (!checkAlive(tribe.tribe.get(i))) {
                System.out.println("\033[33mOrc-" + tribe.tribe.get(i).getClass().getSimpleName() + " " + tribe.tribe.get(i).name
                        + " from the " + tribe.getClass().getSimpleName() + " dead\033[m");
                tribe.tribe.remove(i);
                i--;
            }
        }
    }

    boolean checkAlive(Orc orc) {
        if (orc.hp <= 0)
            return false;
        else
            return true;
    }
}
