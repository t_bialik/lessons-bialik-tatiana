package Orcs;

public abstract class Orc {

    double hp;
    double damage;
    double armor;
    double meal;
    String name;

    public Orc() {
        this.damage = 100;
        this.armor = 100;
        this.meal = 100;
        this.name = RandomName.randomChoice();
    }

    @Override
    public java.lang.String toString() {
        return "Orc-" + this.getClass().getSimpleName() + " " + this.name + "\nHP: " + this.hp + " DAMAGE: " + this.damage + " ARMOR: " + this.armor;
    }

    void attack (Orc enemy) {
        if (this.armor >= enemy.damage) {
            this.hp--;
        }
        else {
            this.hp -= enemy.damage;
        }
    }
}
