package Orcs;

import java.util.Random;

public class RandomName {
    static String [] firstWord = {"Funny","Smart","Fat","Clever","Beautiful","Amazing","Wonderful","Noizy","Lazy","Warm","Hot","Stupid",
            "Disgusting","Fast","Best","Slow","Ugly","Short","Long","Cold","Boring","Old","Young","Sexy","Impossible","Hungry","Angry","Muddy","Awesome",
            "Cruel", "Red", "Purple", "Sunshine", "Windy"};
    static String [] secondWord = {"Max","Tanya","Danya","Andrey","Nastya","Vladik","Mishutka","Vitaliy","Igor","Davud","Phil","Vlad","Iraq",
            "Herman","Roman","Vadim","Leha","Hana","Dimka","Lev","Vita","Sanya","Simon","Nick","Lazar","Yan","Katenka","Irka","Angelina","Yakovka","Lena"};

    static String randomChoice () {
        String name = "";
        Random random = new Random();
        name = firstWord[random.nextInt(firstWord.length)] + " " + secondWord[random.nextInt(secondWord.length)];
        return name;
    }
}
