package Arrays;

import java.util.Random;
import java.util.Scanner;

public class Task25 {
    public static void main(String[] args) {

        Scanner scan = new Scanner (System.in);
        System.out.println("Введи размерность массива");
        int n = scan.nextInt();


        int [][] array = fillArray(n, n);

        System.out.println("Исходная матрица");
        printArray(array, n, n);
        System.out.println("");

        if (findDet(array, n) == 0)
            System.out.println("Обратной матрицы не существует");

        double [][] newArray = new double [n][n];
        int det = findDet(array, n);

        int [][] minArray = arrayMin(array, n);
        int [][] tMin = transArray(minArray, n);

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                newArray[i][j] = (double) tMin [i][j] / det;
            }
        }

        System.out.println("Обратная матрица");
        printdArray (newArray, n, n);
    }

    public static int [][] transArray (int [][] array, int n)  //транспонировать матрицу
    {
        int [][] array1 = new int [n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                array1 [i][j] = array [j][i];
            }
        }
        return array1;
    }

    public static int findDet  (int [][] array, int n)         //найти определитель
    {
        int det = 0;
        int [][] helper = new int [n-1][n-1];

        if (n == 2){
            det = array [0][0] * array[1][1] - array[0][1] * array [1][0];
            return det;
        }

        else
        {
            for (int i = 0; i < n; i++) {

                if (i == 0)
                    for (int j = 0; j < n - 1; j++) {
                        helper [j][0] = array [j + 1][1];
                    }

                for (int j = 0; j < n - 1; j++)
                {
                    for (int k = 0; k < n - 1; k++) {
                        if (k < i)
                            helper [j][k] = array [j + 1][k];
                        if (k > i)
                            helper [j][k] = array [j + 1][k + 1];
                    }
                }

                det += Math.pow (-1, i) * array [0][i] * findDet (helper, n - 1);  //считает определитель
            }
        }
        return det;
    }

    static int findMinor (int [][] array, int i, int j, int n) //минор для элемента
    {
        int [][] min = new int [n - 1][n - 1];
        for (int k = 0; k < n - 1; k++) {
            for (int l = 0; l < n - 1; l++) {
                if (k < i & l < j)
                    min [k][l] =  array [k][l];
                if (k < i & l >= j)
                    min [k][l] = array [k][l + 1];
                if (k >= i & l < j)
                    min [k][l] = array [k + 1][l];
                if (k >= i & l >= j)
                    min [k][l] = array [k + 1][l + 1];
            }
        }
        int result = findDet (min, n - 1);

        return result;
    }

    static int [][] arrayMin (int [][] array, int n)    //матрица миноров
    {
        int [][] minor = new int [n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if ((i + j) % 2 == 0)
                    minor [i][j] = findMinor (array, i, j, n);
                else
                    minor [i][j] = - findMinor(array, i, j, n);
            }
        }
        return minor;
    }

    static int [][] fillArray (int n, int m)         //заполнение массива
    {
        int [][] array = new int [n][m];
        Random rand = new Random();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = rand.nextInt(10);
            }
        }
        return array;
    }

    static void printArray (int [][] array, int n, int m)   //вывод массива
    {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m - 1; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println(array[i][m - 1]);
        }
    }

    static void printdArray (double [][] array, int n, int m)
    {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m - 1; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println(array[i][m - 1]);
        }
    }
}
