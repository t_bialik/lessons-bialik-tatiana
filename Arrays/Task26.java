package Arrays;

import java.util.Random;

public class Task26 {
    public static void main(String[] args) {
        int n = 5;
        int m = 5;
        double [][] array = fillArray (n, m);
        double max = array [0][0];

        System.out.println("Исходная матрица:");
        printArray(array, n, m);
        System.out.println("");

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (array [i][j] > max)
                    max = array [i][j];
            }
        }

        System.out.println("Максимальный элемент " + max);
        System.out.println("");

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array [i][j] /= max;
            }
        }

        System.out.println("Нормированная матрица:");
        printArray (array, n, m);
    }

    static double [][] fillArray (int n, int m)
    {
        double [][] array = new double [n][m];
        Random rand = new Random();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = (int)(rand.nextDouble() * 100);
            }
        }

        return array;
    }

    static void printArray (double [][] array, int n, int m)
    {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m - 1; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println(array[i][m - 1]);
        }
    }
}
