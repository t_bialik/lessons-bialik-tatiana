package Arrays;

import java.util.Random;
import java.util.Scanner;

public class Task14 {
    public static void main(String[] args) {
        int n = 5;
        int[] array = new int [n];
        Random rand = new Random ();

        for (int i = 0; i < array.length; i++) {
            array [i] = rand.nextInt (99);
        }
        Scanner in = new Scanner (System.in);
        int res = 0;

        System.out.println("Введите число");

        int k = in.nextInt();

        for (int el : array)
            if (el <= k)
                res++;

        System.out.println("Количество элементов массива, не превышающих " + k + ": " + res);
    }
}
