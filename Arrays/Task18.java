package Arrays;

import java.util.Random;

public class Task18 {
    public static void main(String[] args) {

        int n = 10;
        int m = 10;
        Random rand = new Random();
        int [][] array = new int [n][m];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = rand.nextInt(99);
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m - 1; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println(array[i][m - 1]);
        }

        int min = array[0][0];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (array[i][j] < min)
                    min = array[i][j];
            }
        }

        System.out.println("Минимальный элемент массива " + min);
    }
}
