package Arrays;

import java.util.Random;

public class Task17 {
    public static void main(String[] args) {
        int k = 5;
        int[] array = new int [k];
        Random rand = new Random ();

        for (int i = 0; i < array.length; i++) {
            array [i] = rand.nextInt (99);
        }
        int max = array [0];

        for (int el : array)
        {
            if (el > max)
                max = el;
        }

        for (double n : array)
        {
            n /= max;
            System.out.println(n);
        }
    }
}
