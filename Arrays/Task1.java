package Arrays;

import java.util.Random;

public class Task1 {
    public static void main(String[] args) {
        int n = 5;
        int[] array = new int [n];
        Random rand = new Random ();

        for (int i = 0; i < array.length; i++) {
            array [i] = rand.nextInt (99);
        }

        printArray (array);

        int min = array [0];

        for (int el : array)
        {
            if (el < min)
                min = el;
        }

        System.out.println("Минимальный элемент массива " + min);
    }

    public static void printArray (int [] array)
    {
        for (int i : array) {
            System.out.print(i + " ");
        }
        System.out.println("");
    }
}
