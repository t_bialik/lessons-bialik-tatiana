package Arrays;

import java.util.Random;

public class Task23 {
    public static void main(String[] args) {
        int n = 5;
        int sum = 0;
        int [][] array = fillArray(n, n);

        printArray (array, n, n);

        for (int i = 0; i < n; i++)
        {
            sum += array [i][n - i - 1];
        }

        System.out.println("Сумма элементов на побочной диагонали " + sum);
    }

    public static int [][] fillArray (int n, int m)
    {
        int [][] array = new int [n][m];
        Random rand = new Random();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = rand.nextInt(99);
            }
        }

        return array;
    }

    public static void printArray (int [][] array, int n, int m)
    {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m - 1; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println(array[i][m - 1]);
        }
    }
}
