package Arrays;

import java.util.Random;

public class Task20 {
    public static void main(String[] args) {
        int n = 3;
        int m = 3;
        int sum = 0;

        int [][] array = fillArray(n, m);
        printArray (array, n, m);

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                sum += array [i][j];
            }
        }

        System.out.println("Сумма всех элементов массива " + sum);
    }

    public static int [][] fillArray (int n, int m)
    {
        int [][] array = new int [n][m];
        Random rand = new Random();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = rand.nextInt(99);
            }
        }

        return array;
    }

    public static void printArray (int [][] array, int n, int m)
    {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m - 1; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println(array[i][m - 1]);
        }
    }
}
