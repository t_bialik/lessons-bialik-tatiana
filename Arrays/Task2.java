package Arrays;

import java.util.Random;

public class Task2 {
    public static void main(String[] args) {
        int n = 5;
        int[] array = new int [n];
        Random rand = new Random ();

        for (int i = 0; i < array.length; i++) {
            array [i] = rand.nextInt (99);
        }

        printArray (array);
        int max = array [0];

        for (int el : array)
        {
            if (el > max)
                max = el;
        }

        System.out.println("Максимальный элемент массива " + max);
    }

    public static void printArray (int [] array)
    {
        for (int i : array) {
            System.out.print(i + " ");
        }
        System.out.println("");
    }
}
