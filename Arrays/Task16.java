package Arrays;

import java.util.Random;

public class Task16 {
    public static void main(String[] args) {

        int n = 5;
        int[] array = new int [n];
        Random rand = new Random ();

        for (int i = 0; i < array.length; i++) {
            array [i] = rand.nextInt (50) - 25;
        }

        for (int i : array) {
            System.out.print(i + " ");
        }
        System.out.println("");

        boolean flag = false;
        int res = array[0];

        for (int i = 0; i < array.length; i++)
        {
            if (array[i] == 0)
            {
                res = 0;
                break;
            }

            if (Math.abs(array[i]) <= Math.abs (res))
            {
                if (array[i] == -res)
                    flag = true;

                res = array[i];
            }
        }

        if (flag)
            System.out.println ("Ближайшие к 0 элементы массива " + res + " и " + -res);
        else
            System.out.println("Ближайший к 0 элемент массива " + res);

    }




}
