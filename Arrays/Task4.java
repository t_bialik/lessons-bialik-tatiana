package Arrays;

import java.util.Random;

public class Task4 {
    public static void main(String[] args) {
        int m = 5;
        int[] array = new int [m];
        Random rand = new Random ();

        for (int i = 0; i < array.length; i++) {
            array [i] = rand.nextInt (99);
        }
        int sum = 0;
        int n = 0;

        for (int i = 0; i < array.length; i++)
        {
            sum += array[i];
            n = i + 1;
        }

        int result = sum/n;

        System.out.println("Среднее арифметическое " + result);
    }
}
