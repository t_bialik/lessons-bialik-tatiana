package Arrays;

import java.util.Random;

public class Task19 {
    public static void main(String[] args) {

        int n = 10;
        int m = 10;

        int [][] array = fillArray (n, m);
        printArray (array, n, m);
        int max = findMax (array, n, m);
        System.out.println("Максимальный элемент массива " + max);
    }

    public static int [][] fillArray (int n, int m)
    {
        int [][] array = new int [n][m];
        Random rand = new Random();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = rand.nextInt(99);
            }
        }

        return array;
    }

    public static void printArray (int [][] array, int n, int m)
    {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m - 1; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println(array[i][m - 1]);
        }
    }

    public static int findMax (int [][] array, int n, int m)
    {
        int max = array[0][0];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (array[i][j] > max)
                    max = array[i][j];
            }
        }

        return max;
    }
}
