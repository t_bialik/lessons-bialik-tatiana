package Arrays;

import java.util.Random;

public class Task6 {
    public static void main(String[] args) {
        int n = 5;
        int[] array = new int [n];
        Random rand = new Random ();

        for (int i = 0; i < array.length; i++) {
            array [i] = rand.nextInt (99);
        }
        int sum = 0;

        for (int i = 1; i < array.length; i += 2) {
                sum += array[i];
        }

        System.out.println("Сумма элементов массива с нечетными номерами " + sum);
    }
}
