package Arrays;

import java.util.Random;

public class Task27 {
    public static void main(String[] args) {
        int n = 5;
        int [][] array = fillArray (n, n);
        int [][] array1 = new int [n][n];

        System.out.println("Исходный массив");
        printArray(array, n, n);
        System.out.println("");

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                array1 [i][j] = array [j][i];
            }
        }

        System.out.println("Транспонированный массив");
        printArray(array1, n, n);
    }

    static int [][] fillArray (int n, int m)
    {
        int [][] array = new int [n][m];
        Random rand = new Random();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = rand.nextInt(99);
            }
        }

        return array;
    }

    static void printArray (int [][] array, int n, int m)
    {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m - 1; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println(array[i][m - 1]);
        }
    }
}
