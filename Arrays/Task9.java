package Arrays;

import java.util.Random;

public class Task9 {
    public static void main(String[] args) {
        int n = 5;
        int[] array = new int [n];
        Random rand = new Random ();

        for (int i = 0; i < array.length; i++) {
            array [i] = rand.nextInt (99);
        }
        long res = 1;

        for (int i = 0; i < array.length; i++) {
            if (i % 2 == 0)
                res *= array[i];
        }

        System.out.println("Произведение элементов массива с четными номерами " + res);
    }
}
