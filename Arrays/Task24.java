package Arrays;

import java.util.Random;
import java.util.Scanner;

public class Task24 {
    public static void main(String[] args) {
        Scanner scan = new Scanner (System.in);
        System.out.println("Введите размерность массива");
        int n = scan.nextInt();
        int [][] array = fillArray(n, n);

        System.out.println("Полученный массив:");
        printArray (array, n, n);

        System.out.println("Определитель матрицы равен " + findDet(array, n));
    }

    static int findDet(int[][] array, int n)
    {
        int det = 0;
        int [][] helper = new int [n-1][n-1];

        if (n == 2){
            det = array [0][0] * array[1][1] - array[0][1] * array [1][0];
            return det;
        }

        else
        {
            for (int i = 0; i < n; i++) {

                if (i == 0)
                    for (int j = 0; j < n - 1; j++) {
                        helper [j][0] = array [j + 1][1];
                    }

                for (int j = 0; j < n - 1; j++)         //миноры 0 строки
                {
                    for (int k = 0; k < n - 1; k++) {
                        if (k < i)
                            helper [j][k] = array [j + 1][k];
                        if (k > i)
                            helper [j][k] = array [j + 1][k + 1];
                    }
                }
                    det += Math.pow (-1, i) * array [0][i] * findDet (helper, n - 1);  //считает определитель
            }
        }
        return det;
    }

    static int [][] fillArray (int n, int m)
    {
        int [][] array = new int [n][m];
        Random rand = new Random();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = rand.nextInt(10);
            }
        }

        return array;
    }

    static void printArray (int [][] array, int n, int m)
    {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m - 1; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println(array[i][m - 1]);
        }
    }
}
