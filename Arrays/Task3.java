package Arrays;

import java.util.Random;

public class Task3 {
    public static void main(String[] args) {
        int n = 5;
        int[] array = new int [n];
        Random rand = new Random ();

        for (int i = 0; i < array.length; i++) {
            array [i] = rand.nextInt (99);
        }
        int sum = 0;

        for (int el : array)
            sum +=el;

        System.out.println("Сумма элементов массива " + sum);
    }
}
