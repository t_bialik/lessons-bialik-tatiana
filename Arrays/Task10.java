package Arrays;

import java.util.Random;

public class Task10 {
    public static void main(String[] args) {
        int n = 5;
        int[] array = new int [n];
        Random rand = new Random ();

        for (int i = 0; i < array.length; i++) {
            array [i] = rand.nextInt (99);
        }
        int res = 0;

        for (int el : array) {
            if (el == 0)
                res++;
        }

        System.out.println("Количество 0 в массиве " + res);
    }
}
