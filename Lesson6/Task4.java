package Lesson6;

import java.util.Random;

public class Task4 {
    public static void main(String[] args) {
        int n = 5;
        int m = 4;
        int [][] array = fillArray(n, m);
        System.out.println("Исходный массив");
        printArray(array);

        sortArray (array);
        System.out.println("Отсортированный массив");
        printArray(array);
    }

    static int sumColumn (int [][] array, int column)
    {
        int result = 0;
        for (int i = 0; i < array.length; i++) {
            result += array [i][column];
        }
        return result;
    }

    static void sortArray(int [][] array)
    {
        int p = 0;
        for (int k = 0; k < array[0].length; k ++)
        {
            for (int i = 0; i < array[0].length - 1; i++) {
                if (sumColumn (array, i) < sumColumn (array, i + 1))
                {
                    for (int j = 0; j < array.length; j++) {
                        p = array [j][i];
                        array [j][i] = array [j][i + 1];
                        array [j][i + 1] = p;
                    }
                }
            }
        }
    }

    static int [][] fillArray (int n, int m)
    {
        int [][] array = new int [n][m];
        Random rand = new Random();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = (int)(rand.nextDouble() * 10);
            }
        }

        return array;
    }

    static void printArray (int [][] array)
    {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length - 1; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println(array[i][array[0].length - 1]);
        }
        System.out.println("");
    }
}
