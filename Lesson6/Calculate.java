package Lesson6;

public class Calculate {
    public static void main(String[] args) {
        int summa = sum (3,5);
        int minus = minus (5, 8);
        int mp = multiply (87, 93);

        System.out.println(summa + " " + minus + " " + mp);
    }

    static int sum (int x, int y)
    {
        return x + y;
    }

    static int minus (int x, int y)
    {
        return x - y;
    }

    static int multiply (int x, int y)
    {
        return x * y;
    }
}

