package Lesson6;

import java.util.Random;

public class Task1 {
    public static void main(String[] args) {
        int n = 5;
        int [][] array = fillArray(n, n);
        System.out.println("Исходный массив");
        printArray(array);

//        for (int i = 0; i < n; i++) {
//            for (int j = 0; j < n; j++) {
//                for (int k = 0; k < n; k++) {
//                    for (int l = 0; l < n; l++) {
//                        if (array [k][l] > array [i][j])
//                        {
//                            p = array [k][l];
//                            array [k][l] = array [i][j];
//                            array [i][j] = p;
//                        }
//                    }
//                }
//            }
//        }
//        System.out.println("Сортировка по возрастанию");
//        printArray(array, n, n);
//        System.out.println("");
//
//        for (int i = 0; i < n; i++) {
//            for (int j = 0; j < n; j++) {
//                for (int k = 0; k < n; k++) {
//                    for (int l = 0; l < n; l++) {
//                        if (array [k][l] < array [i][j])
//                        {
//                            p = array [k][l];
//                            array [k][l] = array [i][j];
//                            array [i][j] = p;
//                        }
//                    }
//                }
//            }
//        }
//        System.out.println("Сортировка по убыванию");
//        printArray(array, n, n);
        sortUp (array);
        sortDown (array);
    }

    static int [][] fillArray (int n, int m)
    {
        int [][] array = new int [n][m];
        Random rand = new Random();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = (int)(rand.nextDouble() * 100);
            }
        }

        return array;
    }

    static void printArray (int [][] array)
    {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length - 1; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println(array[i][array[0].length - 1]);
        }
        System.out.println("");
    }

    static void sortUp (int [][] array)
    {
        int p = 0;
        for (int i = 0; i < array.length * array[0].length; i++) {
            for (int j = 0; j < array.length; j++) {
                for (int k = 0; k < array[0].length - 1; k++) {
                    if (array [j][k] > array [j][k + 1])
                    {
                        p = array [j][k];
                        array [j][k] = array [j][k + 1];
                        array [j][k + 1] = p;
                    }
                }

                if (j == array.length - 1)
                    continue;
                if (array [j][array[0].length - 1] > array [j + 1][0])
                {
                    p = array [j][array[0].length - 1];
                    array [j][array[0].length - 1] = array [j + 1][0];
                    array [j + 1][0] = p;
                }
            }
        }
        System.out.println("Сортировка по возрастанию");
        printArray(array);
    }

    static void sortDown (int [][] array)
    {
        int p = 0;
        for (int i = 0; i < array.length * array[0].length; i++) {
            for (int j = 0; j < array.length; j++) {
                for (int k = 0; k < array[0].length - 1; k++) {
                    if (array [j][k] < array [j][k + 1])
                    {
                        p = array [j][k];
                        array [j][k] = array [j][k + 1];
                        array [j][k + 1] = p;
                    }
                }

                if (j == array.length - 1)
                    continue;
                if (array [j][array[0].length - 1] < array [j + 1][0])
                {
                    p = array [j][array[0].length - 1];
                    array [j][array[0].length - 1] = array [j + 1][0];
                    array [j + 1][0] = p;
                }
            }
        }
        System.out.println("Сортировка по убыванию");
        printArray(array);
    }
}
