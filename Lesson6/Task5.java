package Lesson6;

import java.util.Random;

public class Task5 {
    public static void main(String[] args) {
        int n = 5;
        int m = 4;
        int [][] array = fillArray(n, m);

        System.out.println("Исходный массив");
        printArray(array);

        sortArray(array);
        System.out.println("Отсортированный массив");
        printArray(array);
    }

    static int sumString (int [] string)
    {
        int result = 0;
        for (int i = 0; i < string.length; i++) {
            if (string[i] % 2 == 0)
                result += string[i];
        }
        return result;
    }

    static void sortArray(int [][] array)
    {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (sumString (array[j]) > sumString (array [j + 1]))
                {
                    int [] temp = array [j];
                    array [j] = array [j + 1];
                    array [j + 1] = temp;
                }
            }
        }
    }

    static int [][] fillArray (int n, int m)
    {
        int [][] array = new int [n][m];
        Random rand = new Random();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = (int)(rand.nextDouble() * 10);
            }
        }

        return array;
    }

    static void printArray (int [][] array)
    {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length - 1; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println(array[i][array[0].length - 1]);
        }
        System.out.println("");
    }
}

