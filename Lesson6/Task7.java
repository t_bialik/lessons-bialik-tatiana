package Lesson6;

import java.util.Random;

public class Task7 {
    public static void main(String[] args) {
        int n = 5;
        int [][] array = fillArray (n, n);
        printArray (array);

        System.out.println("Количество строк, содержащих простые числа: " + numString (array));
        System.out.println("Количество столбцов, содержащих простые числа: " + numColumn(array));
    }

    static boolean checkSimple (int a)
    {
        boolean flag = true;
        for (int i = 2; i < a / 2; i++) {
            if (a % i == 0)
                flag = false;
        }
        return flag;
    }

    static int numString (int [][] array)
    {
        int count = 0;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                if (checkSimple(array [i][j]))
                {
                    count++;
                    break;
                }
            }
        }
        return count;
    }

    static int numColumn (int [][] array)
    {
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                if (checkSimple(array [j][i]))
                {
                    count++;
                    break;
                }
            }
        }
        return count;
    }

    static void printArray (int [][] array)
    {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length - 1; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println(array[i][array[0].length - 1]);
        }
        System.out.println("");
    }

    static int [][] fillArray (int n, int m)
    {
        int [][] array = new int [n][m];
        Random rand = new Random();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = (int)(rand.nextDouble() * 100);
            }
        }

        return array;
    }
}
