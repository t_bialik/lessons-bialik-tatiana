package Lesson6;

import java.util.Random;

public class Task9 {
    public static void main(String[] args) {
        int n = 5;
        int m = 4;
        int d = 3;
        int [][] matrix_1 = fillArray (n, m);
        int [][] matrix_2 = fillArray (m, d);

        System.out.println("Массив №1");
        printArray (matrix_1);
        System.out.println("Массив №2");
        printArray (matrix_2);
        System.out.println("Результирующий массив");
        printArray (multMatrix(matrix_1, matrix_2));
    }

    static int [][] multMatrix (int [][] matrix_1, int [][] matrix_2)
    {
        int [][] resultMatrix = new int [matrix_1.length][matrix_2[0].length];

        for (int i = 0; i < matrix_1.length; i++) {
            for (int j = 0; j < matrix_1[0].length; j++) {
                for (int k = 0; k < matrix_2[0].length; k++) {
                    resultMatrix[i][k] += matrix_1 [i][j] * matrix_2 [j][k];
                }
            }
        }
        return resultMatrix;
    }


    static int [][] fillArray (int n, int m)
    {
        int [][] array = new int [n][m];
        Random rand = new Random();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = (int)(rand.nextDouble() * 10);
            }
        }

        return array;
    }

    static void printArray (int [][] array)
    {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length - 1; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println(array[i][array[0].length - 1]);
        }
        System.out.println("");
    }
}
