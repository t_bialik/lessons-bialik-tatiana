package Lesson15;

public class Task1 {

    public static void main(String[] args) {
        printNums(10);
    }

    static int printNums(int n) {
        if (n == 0)
            return 0;
        System.out.println(n);
        printNums(n - 1);
        return 0;
    }
}
