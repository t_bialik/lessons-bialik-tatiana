package Lesson15;

public class Task4 {

    public static void main(String[] args) {
        System.out.println(sum(360782));
    }

    static int sum(int n) {
        if (n < 10)
            return n;
        else
            return n % 10 + sum(n / 10);
    }
}

//8:24 9:13 10:07
//9:10 9:55 10:45
