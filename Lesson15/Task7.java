package Lesson15;

public class Task7 {

    public static void main(String[] args) {
        fact(189, 2);
    }


    static int fact(int n, int d) {
        if (n % d == 0) {
            System.out.println(d);

            if (d >= n / 2)
                return 0;

            return fact(n / d, d);
        } else {
            if (d == 2)
                return fact(n, ++d);
            else
                return fact(n, d + 2);
        }
    }
}
