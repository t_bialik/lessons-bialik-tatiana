package Lesson15;

import java.util.Scanner;

public class Task8 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            String str = scanner.nextLine();
            System.out.println(palindrom(str));
        }
    }


    static boolean palindrom (String str) {

        if (str.length() <= 1)
            return true;

        if (str.charAt(0) != str.charAt(str.length() - 1))
            return false;
        else {
            return palindrom(str.substring(1, str.length() - 1));
        }
    }
}
