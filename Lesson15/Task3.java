package Lesson15;

public class Task3 {

    public static void main(String[] args) {
        System.out.println(accerman(3,10));
    }


    static int accerman (int m, int n) {

        if (m == 0)
            return n + 1;
         if (m > 0 && n == 0)
             return accerman(m - 1, 1);
         else
             return accerman(m - 1, accerman(m, n - 1));
    }
}
