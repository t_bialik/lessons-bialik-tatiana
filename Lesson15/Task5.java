package Lesson15;

public class Task5 {

    public static void main(String[] args) {
        print(345890);
    }

    static int print (int n) {
        if (n < 1) {
            return 0;
        }

        System.out.println(n % 10);
        return print(n / 10);

    }
}
