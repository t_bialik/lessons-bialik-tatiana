package Lesson15;

public class Task2 {

    public static void main(String[] args) {
        fromAtoB(13,8);
    }

    static int fromAtoB (int a, int b) {

        if (a == b) {
            System.out.println(a);
            return 0;
        }

        if (a > b) {
            System.out.println(a);
            fromAtoB(a - 1, b);
        }
        else {
            System.out.println(a);
            fromAtoB(a + 1, b);
        }

        return 0;
    }
}
