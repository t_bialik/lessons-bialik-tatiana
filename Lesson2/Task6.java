package Lesson2;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Задание 6");
        System.out.println("Введите номер месяца");
        int month = in.nextInt();
        if (month <= 2 || month == 12)
            System.out.println("Зима");
        if (month >= 3 & month <= 5)
            System.out.println("Весна");
        if (month >= 6 & month <= 8)
            System.out.println("Лето");
        if (month >= 9 & month <= 11)
            System.out.println("Осень");
    }
}
