package Lesson2;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Задание 8");
        System.out.println("Введи число");

        int numb = in.nextInt();
        boolean f = false;

        for (int i = 2; i < numb/2; i++) {
            if (numb % i == 0) {
                System.out.println("Число составное");
                f = true;
                break;
            }
        }
        if (!f)
            System.out.println("Число простое");
    }
}
