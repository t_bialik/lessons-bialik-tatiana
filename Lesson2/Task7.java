package Lesson2;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Задание 7");
        System.out.println("Введи день");
        int number = in.nextInt();
        if (number >= 1 & number <= 10)
            System.out.println("1ая декада");
        if (number >= 11 & number <= 20)
            System.out.println("2ая декада");
        if (number >= 21 & number <= 31)
            System.out.println("3ая декада");
    }
}
