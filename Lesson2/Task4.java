package Lesson2;

public class Task4 {
    public static void main(String[] args) {
        System.out.println("Задание 4");
        four_t(15);

    }

    public static void four_t(int min) {
        if (min >= 0 & min < 16)
            System.out.println("1-ая четверть");
        if (min >= 16 & min < 31)
            System.out.println("2-ая четверть");
        if (min >= 31 & min < 46)
            System.out.println("3-ая четверть");
        if (min >= 46 & min < 59)
            System.out.println("4-ая четверть");
    }
}
