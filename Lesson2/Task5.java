package Lesson2;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Задание 5");
        System.out.println("Введите номер сезона");
        int num = in.nextInt();
        String result;
        switch (num) {
            case 1:
                result = "Зима";
                System.out.println(result);
                break;
            case 2:
                result = "Весна";
                System.out.println(result);
                break;
            case 3:
                result = "Лето";
                System.out.println(result);
                break;
            case 4:
                result = "Осень";
                System.out.println(result);
                break;
        }
    }
}
