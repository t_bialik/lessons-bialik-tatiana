package Lesson2;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Задание 9");
        System.out.println("Длина треугольника:");
        int d = in.nextInt();
        for (; d > 0; d--) {
            for (int i = d; i > 0; i--) {
                System.out.print("*");
            }
            System.out.println(" ");
        }
    }
}
