package Lesson2;

public class Task2 {
    public static void main(String[] args) {
        System.out.println("Задание 2");
        sec_t(1);
        sec_t(0);
        sec_t(-3);
    }

    static void sec_t(int a) {
        if (a == 0)
            System.out.println("Верно");
        else
            System.out.println(a + ": неверно");
    }
}
